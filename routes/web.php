<?php
use Illuminate\Support\Facades\Route;
use App\Providers\RouteServiceProvider;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect('/login');
});

Route::get('/clear-cache-all', function() {
    Artisan::call('cache:clear');
    dd("Cache Clear All");
});

Route::post('sendFax', [App\Http\Controllers\LetterController::class, 'sendfax'])->name('sendFax');
Route::post('sendLetter', [App\Http\Controllers\LetterController::class, 'sendletter'])->name('sendLetter');
Route::get('testLetter', [App\Http\Controllers\LetterController::class, 'testletter'])->name('testLetter');
Route::get('testHumbleFaxLetter', [App\Http\Controllers\LetterController::class, 'testHumbleFaxLetter'])->name('testHumbleFaxLetter');

Route::get('lobAddress', [App\Http\Controllers\UserController::class, 'getLobAddress'])->name('lobAddress');
Route::get('getLobByID', [App\Http\Controllers\UserController::class, 'getLobByID'])->name('getLobByID');
Route::get('getHumbleByID', [App\Http\Controllers\UserController::class, 'getHumbleByID'])->name('getHumbleByID');
Route::get('faxAddress', [App\Http\Controllers\UserController::class, 'getFaxAddress'])->name('faxAddress');
Route::post('saveLobAddress', [App\Http\Controllers\UserController::class, 'storeLobAddress'])->name('saveLobAddress');
Route::post('updateLobAddress', [App\Http\Controllers\UserController::class, 'updateLobAddress'])->name('updateLobAddress');
Route::post('updateHumbleAddress', [App\Http\Controllers\UserController::class, 'updateHumbleAddress'])->name('updateHumbleAddress');
Route::post('saveFaxAddress', [App\Http\Controllers\UserController::class, 'storeFaxAddress'])->name('saveFaxAddress');
Route::get('userLetters', [App\Http\Controllers\UserLetterController::class, 'getLetter'])->name('userLetters');
Route::get('userLetterCount', [App\Http\Controllers\UserLetterController::class, 'getCount'])->name('userLetterCount');

Route::get('allLobLetters', [App\Http\Controllers\LetterController::class, 'getLobLetters'])->name('allLobLetters');
Route::get('pendingLetters', [App\Http\Controllers\LetterController::class, 'getPendingLetters'])->name('pendingLetters');
Route::get('allHumbleLetters', [App\Http\Controllers\LetterController::class, 'getHumbleLetters'])->name('allHumbleLetters');
Route::get('tmp', [App\Http\Controllers\LetterController::class, 'tmpQueue'])->name('tmp');
Route::get('humbleletterstatus', [App\Http\Controllers\LetterController::class, 'getHumblebyLetterStatus'])->name('humbleletterstatus');
Route::get('lobletterstatus', [App\Http\Controllers\LetterController::class, 'getLobbyLetterStatus'])->name('lobletterstatus');

/******************************/
Route::get('backend/token', function (Request $request) {
    $token = $request->session()->token();

    $token = csrf_token();

    // ...
});

Route::get('backend/setting', [App\Http\Controllers\AdminSettingController::class, 'index'])->name('list.admin_setting');
Route::get('backend/admin_setting/edit/{id}', [App\Http\Controllers\AdminSettingController::class, 'edit'])->name('edit.admin_setting');
Route::post('backend/admin_setting/edit/{id}', [App\Http\Controllers\AdminSettingController::class, 'update'])->name('edit.admin_setting');

Route::get('backend/login', [App\Http\Controllers\AdminLoginController::class, 'index'])->name('adminlogin');
Route::get('backend/admindashboard', [App\Http\Controllers\AdminDashboardController::class, 'index'])->name('admin');

Route::get('backend/lob_address', [App\Http\Controllers\LobAddressController::class, 'index'])->name('list.lob_address');
Route::get('backend/lob_address/add', [App\Http\Controllers\LobAddressController::class, 'create'])->name('add.lob_address');
Route::post('backend/lob_address/add', [App\Http\Controllers\LobAddressController::class, 'store'])->name('add.lob_address');
Route::get('backend/lob_address/edit/{id}', [App\Http\Controllers\LobAddressController::class, 'edit'])->name('edit.lob_address');
Route::post('backend/lob_address/edit/{id}', [App\Http\Controllers\LobAddressController::class, 'update'])->name('edit.lob_address');

Route::get('backend/letter_stream', [App\Http\Controllers\LetterStreamController::class, 'index'])->name('list.letter_stream');
Route::get('backend/letter_stream/add', [App\Http\Controllers\LetterStreamController::class, 'create'])->name('add.letter_stream');
Route::post('backend/letter_stream/add', [App\Http\Controllers\LetterStreamController::class, 'store'])->name('add.letter_stream');
Route::get('backend/letter_stream/edit/{id}', [App\Http\Controllers\LetterStreamController::class, 'edit'])->name('edit.letter_stream');
Route::post('backend/letter_stream/edit/{id}', [App\Http\Controllers\LetterStreamController::class, 'update'])->name('edit.letter_stream');

Route::get('backend/humblex_address', [App\Http\Controllers\HumblexAddressController::class, 'index'])->name('list.humblex_address');
Route::get('backend/humblex_address/add', [App\Http\Controllers\HumblexAddressController::class, 'create'])->name('add.humblex_address');
Route::post('backend/humblex_address/add', [App\Http\Controllers\HumblexAddressController::class, 'store'])->name('add.humblex_address');
Route::get('backend/humblex_address/edit/{id}', [App\Http\Controllers\HumblexAddressController::class, 'edit'])->name('edit.humblex_address');
Route::post('backend/humblex_address/edit/{id}', [App\Http\Controllers\HumblexAddressController::class, 'update'])->name('edit.humblex_address');

Route::get('backend/user', [App\Http\Controllers\AdminUserController::class, 'index'])->name('adminuser');

Route::get('backend/admin_user/edit/{id}', [App\Http\Controllers\AdminUserController::class, 'edit'])->name('edit.admin_user');
Route::post('backend/admin_user/edit/{id}', [App\Http\Controllers\AdminUserController::class, 'update'])->name('edit.admin_user');

Route::get('backend/changepassword', [App\Http\Controllers\AdminProfileController::class, 'getReset'])->name('admin.changepassword');
Route::post('backend/changepassword', [App\Http\Controllers\AdminProfileController::class, 'postReset'])->name('admin.changepassword');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

/* Emailtemplate Management  */
Route::get('backend/emailtemplate', [App\Http\Controllers\EmailtemplateController::class, 'index'])->name('list.emailtemplate');
Route::get('backend/emailtemplate/add', [App\Http\Controllers\EmailtemplateController::class, 'create'])->name('add.emailtemplate');
Route::post('backend/emailtemplate/add', [App\Http\Controllers\EmailtemplateController::class, 'store'])->name('add.emailtemplate');
Route::get('backend/emailtemplate/edit/{id}', [App\Http\Controllers\EmailtemplateController::class, 'edit'])->name('edit.emailtemplate');
Route::post('backend/emailtemplate/edit/{id}', [App\Http\Controllers\EmailtemplateController::class, 'update'])->name('edit.emailtemplate');
Route::get('backend/emailtemplate/{id}/delete', [App\Http\Controllers\EmailtemplateController::class, 'destroy'])->name('delete.emailtemplate');