<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/


Route::get('getUserSetting', [App\Http\Controllers\UserController::class, 'getSetting'])->name('getUserSetting');
Route::post('saveUserSetting', [App\Http\Controllers\UserController::class, 'storeSetting'])->name('saveUserSetting');
Route::post('deleteFaxAddress', [App\Http\Controllers\UserController::class, 'deleteFaxAddress'])->name('deleteFaxAddress');
Route::post('deleteLobAddress', [App\Http\Controllers\UserController::class, 'deleteLobAddress'])->name('deleteLobAddress');
Route::post('updateLobAddress', [App\Http\Controllers\UserController::class, 'updateLobAddress'])->name('updateLobAddress');
Route::post('updateHumbleAddress', [App\Http\Controllers\UserController::class, 'updateHumbleAddress'])->name('updateHumbleAddress');
Route::post('sendFax', [App\Http\Controllers\LetterController::class, 'sendfax'])->name('sendFax');
Route::post('sendLetter', [App\Http\Controllers\LetterController::class, 'sendletter'])->name('sendLetter');
Route::get('lobAddress', [App\Http\Controllers\UserController::class, 'getLobAddress'])->name('lobAddress');
Route::get('lobAddresslatest', [App\Http\Controllers\UserController::class, 'getLobAddressLatest'])->name('lobAddresslatest');
Route::get('faxAddress', [App\Http\Controllers\UserController::class, 'getFaxAddress'])->name('faxAddress');
Route::get('faxAddresslatest', [App\Http\Controllers\UserController::class, 'getFaxAddressLatest'])->name('faxAddresslatest');
Route::post('saveLobAddress', [App\Http\Controllers\UserController::class, 'storeLobAddress'])->name('saveLobAddress');
Route::post('saveStreamAddress', [App\Http\Controllers\UserController::class, 'storeStreamAddress'])->name('saveStreamAddress');
Route::post('saveFaxAddress', [App\Http\Controllers\UserController::class, 'storeFaxAddress'])->name('saveFaxAddress');
Route::get('userLetters', [App\Http\Controllers\UserLetterController::class, 'getLetter'])->name('userLetters');
Route::get('userLetterCount', [App\Http\Controllers\UserLetterController::class, 'getCount'])->name('userLetterCount');
Route::get('allLobLetters', [App\Http\Controllers\LetterController::class, 'getLobLetters'])->name('allLobLetters');
Route::get('pendingLetters', [App\Http\Controllers\LetterController::class, 'getPendingLetters'])->name('pendingLetters');
Route::get('allHumbleLetters', [App\Http\Controllers\LetterController::class, 'getHumbleLetters'])->name('allHumbleLetters');
Route::get('myLetters', [App\Http\Controllers\UserLetterController::class, 'myLetters'])->name('myLetters');


