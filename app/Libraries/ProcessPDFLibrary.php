<?php

namespace App\Libraries;

use \App\Libraries\LobLibrary;
use \App\Libraries\HumbleFaxLibrary;

use App\Models\Letter;
use Log;

class ProcessPDFLibrary
{

    private $letter;
    private $action;

    public function __construct(){
    }

    public function setLetter( $lid ){
        $this->letter = Letter::find($lid);
    }

    public function setAction( $action ){
        $this->action = $action;
    }

    /**
     * Save dummy data.
     *
     * @param  \Lob\Lob  $lob
     * @return void
     */
    public function createSendPDF(){
        if( $this->letter && $this->action ){
            $letterId = $this->letter->id;
            $letterContent = $this->letter->ext_letter_content;
            if( $letterId && $letterContent ){
                Log::info('Letter PDF creation PDF Jobs: '.$this->letter->cid);
                $mpdf = new \Mpdf\Mpdf( ['mode' => 'utf-8', 'format' => [215.89, 279.4] ] );
                $mpdf->WriteHTML( $letterContent );
                $pdfFile = "Letter_{$letterId}.pdf";
                // Log::info('Letter PDF PATH: '."app/public/mpdf/".$pdfFile);
                $fileName = storage_path("app/public/mpdf/".$pdfFile);
                $mpdf->Output( $fileName, 'F' );
                $this->letter->pdf_file = $pdfFile;
                $this->letter->save();
                if( $this->action == 'letter' ){
                    $lob = new LobLibrary();
                    $lob->sendLetter( $this->letter );
                }
                if( $this->action == 'fax' && $this->letter->bureauname){
                    $hfax = new HumbleFaxLibrary( $this->letter->bureauname );
                    $hfax->sendFax( $this->letter );
                }
            }
        }
        $this->letter = '';
        $this->action = '';
    }
}
