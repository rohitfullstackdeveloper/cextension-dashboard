<?php

namespace App\Libraries;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ConnectException;
use \App\Models\Letter;
use \App\Models\LobAddress;
use \App\Models\LetterUser;
use DB;
class LetterStreamLibrary
{

    private $api_Id;
    private $api_Key;

    private $unique_id;
    private $hash;

    public function __construct($api_id, $api_key)
    {
        $this->api_Id = $api_id;
        $this->api_Key = $api_key;
        // $this->api_Id = env('LSREAM_ID', false);
        // $this->api_Key = env('LSREAM_KEY', false);
        $this->client = new Client(array('base_uri' => 'https://www.letterstream.com/apis/'));
    }

    /**
     * Save dummy data.
     *
     * @param  \Lob\Lob  $lob
     * @return void
     */
    public function storeDummy(){
        /*
        to[]: (required) an array of recipients addresses shall be formatted as described in tables 2.2 and 2.3, a
        from: (required) from/return address (max of 1 from address)
        single_file:(required) the pdf to be mailed – this single file will be sent to every recipient in the “to” argument
        The file can be transferred as a multipart/form-data file element or as blob. Base64 encoding may be necessary for the latter.
        pages:(required) number of pages in file
        mailtype:(optional defaults to firstclass)
        Type of mail ('firstclass',’firstclass_hse’,'certified',’certnoerr’, 'postcard','flat',’propostcard’)
        coversheet: (optional defaults to true) True/false shall we create a coversheet or is your pdf formatted to fit our envelopes
        duplex: (optional defaults to simplex) (Y)duplex|(N)simplex
        ink: (optional defaults to B) (B)lack|(C)olor
        paper: (optional defaults to W) paper options listed above
        returnenv: (optional defaults to N) options are Y|9RWS|9LWS|634|N
        */
        $timeStamp = time();
        $string_to_hash =  substr( $timeStamp, -6 ).$this->api_Key.substr( $timeStamp, 6);
        $hashKey = md5(base64_encode($string_to_hash));
        $response = $this->client->request('POST', 'https://www.letterstream.com/apis/', [
            'form_params' => [
                'a' => $this->api_Id,
                'h' => $hashKey,
                't' => $timeStamp,
                'job' => 'job_'.$timeStamp,
                'to[]' => $timeStamp.':John:Doe:123 S Sunny St:Suite 101:Scottsdale:AZ:85281',
                'pages' => 1,
                'from' => 'John:Doe:123 S Sunny St:Suite 101:Scottsdale:AZ:85281',
                'single_file' => 'path_to_file'
            ]
        ]);
    }

    public function sendLetterStream( Letter $letter ){
        $company = LobAddress::where('name', $letter->bureauname)->first();
        $client = LetterUser::where('user_id', $letter->cid)->first();

        if(empty( $company )){
            $message = 'To Address is Missing. Please update your credentials for '.$letter->bureauname; 
            $this->failedLetterNotification($letter,$message); 
            $response[ 'success' ] = 0;
            $response[ 'msg' ] = $message;
        }else if( empty( $client )){
            $message = 'Client address is missing for '.$letter->bureauname;
            $this->failedLetterNotification($letter,$message); 
            $response[ 'success' ] = 0;
            $response[ 'msg' ] = $message;
        }else{
            $lsubject = $letter->subject;
            $subject = substr($lsubject, 0, 20);
            $pdf_file = $letter->pdf_file;
            $api_id = $this->api_Id;
            $api_key = $this->api_Key;
            $unique_id = time();
            $to_address = $company->company.':'.$company->name.':'.$company->address_line1.':'.$company->address_line2.':'.$company->address_city.':'.$company->address_state.':'.$company->address_zip;
            $sender_name  = $client->firstname.':'.$client->lastname;        
            $from_address=  $sender_name.':'.$client->street_address.':'.$client->city.':'.$client->state.':'.$client->zip;     
            $string_to_hash = substr($unique_id,-6).$api_key.substr($unique_id,0,6);
            $hash = md5(base64_encode($string_to_hash));
            $data = array('a' => $api_id, 'h' => $hash, 't'=> $unique_id );
            
            $file_location = base_path()."/public/attachment_output/".$pdf_file;
            $no_of_pages = preg_match_all("/\/Page\W/", file_get_contents( $file_location ),$dummy);
            $file_object = base64_encode( file_get_contents( $file_location ));
    
            $data['single_file'] = $file_object;
            $data['job'] =  $subject.' '.date('h:i:s');
            $data['pages'] = $no_of_pages;
            $data['from'] = $from_address;
            $data['to[]'] = $unique_id.':'.$to_address;
            $data['mailtype'] = 'firstclass';
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://www.letterstream.com/apis/');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $ch_response = curl_exec($ch);
            curl_close($ch);   
            
            $striphtml =strip_tags($ch_response);
            $tag_output = htmlspecialchars_decode($striphtml);
            $preg_output = preg_replace('/(\v|\s)+/', ' ', $tag_output);
            // echo '<pre>';print_r($preg_output);exit;
            if ( preg_match("/SuccessTestMode/", $ch_response) ) {
                $letter->letter_status = 1;
                $letter->save();
                $response[ 'success' ] = 1;
                $response[ 'msg' ] = 'Letter have been sent successfully.';
            }else{
                $response[ 'success' ] = 0;
                $message = $preg_output;
                $response[ 'msg' ] = $message;
                
                $this->failedLetterNotification($letter,$$message); 
            }
        }
        
        echo json_encode( $response,JSON_UNESCAPED_SLASHES ); 
        
    }
    
    public function failedLetterNotification( $letter, $message ){
        
        DB::table('failed_letters')->insert(
            ['failed_letter_id' => $letter->id, 'letter_author_id' => $letter->user_id, 'letter_author_email' => $letter->user_email, 'error_mesage' => $message]
        );  
        
        $find = array(
            '[user_name]',  
        );
        
        $replace = array(
            'user_name' => $letter->user_name,
        );
        
        $adminsettings = DB::table('admin_settings')->where('id', '1')->get();
        $admin_email = $adminsettings[0]->admin_email;
        $email_template_id = $adminsettings[0]->email_template_id;

        $emailtemplates = DB::table('emailtemplates')->where('id', $email_template_id)->get();
        $subject = $emailtemplates[0]->subject;
        // $format = strip_tags($emailtemplates[0]->message);
        $format = $emailtemplates[0]->message;
        
        $failure_message = str_replace(array("\r\n", "\r", "\n"), '', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '', trim(str_replace($find, $replace, $format))));
        // Mail Send
        $from = $admin_email;
        $to = $letter->user_email;
        $subject = $subject;
        $message = $failure_message;
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        $headers .= "From:" . $from . "\r\n";
        
        $mail_status = mail($to,$subject,$message, $headers);
    }
}
