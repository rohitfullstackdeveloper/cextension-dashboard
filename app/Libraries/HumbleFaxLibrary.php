<?php
namespace App\Libraries;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ConnectException;
use \App\Models\Letter;
use \App\Models\Humblefax;
use \App\Models\LetterUser;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use DB;
class HumbleFaxLibrary{
    private $destination,$from_humblefax_email;
    public function __construct( $letter,$from_email ){
        $humbleFax = Humblefax::where('slug', 'LIKE', "%{$letter->bureauname}%")->where('user_id', '=', $letter->user_id)->first();

        if( $humbleFax ){
            $this->destination = $humbleFax;
        }
        // echo '<pre>'; print_r($humbleFax); exit;
        $this->from_humblefax_email = $from_email;
    }

    public function sendFax(Letter $letter, $user_email ){
        // echo '<pre>'; print_r($letter->bureauname); exit;
        if( !$this->destination ){
            $this->humbleFaxError( $letter );
            $response[ 'success' ] = 0;
            $response[ 'msg' ] = 'Letter have been not sent successfully';
            echo json_encode( $response,JSON_UNESCAPED_SLASHES ); exit;
        }

        $letterUser = LetterUser::where( 'user_id', $letter->cid )->first();
        $name = trim("{$letterUser->firstname} {$letterUser->middlename} {$letterUser->lastname} {$letterUser->suffix}");
        // $from = "vs@vs.com";
        // $toAddress = "vishalsuri.mcc@gmail.com";

        $from = $letterUser->email;
        $from = $this->from_humblefax_email;
        $toAddress = $this->destination->faxemail;
        $subject = "Testing";
        $file = URL::to('/'). "/attachment_output/".$letter->pdf_file;
        $mailMessage = '<html>
                        <head>
                            <title>'.$name.'</title>
                        </head>
                        <body>
                        <p>Sender Address!</p>
                        <table>
                            <tr> <td>Name</td><td>'.$name.'</td> </tr>
                            <tr> <td>Country</td><td>US</td> </tr>
                            <tr> <td>Address</td><td>'.$letterUser->street_address.'</td> </tr>
                            <tr> <td>City</td><td>'.$letterUser->city.'</td> </tr>
                            <tr> <td>State</td><td>'.$letterUser->state.'</td> </tr>
                            <tr> <td>Zip</td><td>'.$letterUser->zip.'</td> </tr>
                            <tr> <td>Email</td><td>'.$letterUser->email.'</td> </tr>
                            <tr> <td>Phone</td><td>'.$letterUser->phone_home.'</td> </tr>
                        </table>
                        </body>
                    </html>';

        $subject = "$name {$this->destination->name}";
        $fileAttachment = trim($file);
        $pathInfo       = pathinfo($fileAttachment);
        $attchmentName  = "attachment_".date("YmdHms").( (isset($pathInfo['extension']))? ".".$pathInfo['extension'] : ""
        );
        $attachment    = chunk_split(base64_encode(file_get_contents($fileAttachment)));
        $boundary      = "PHP-mixed-".md5(time());
        $boundWithPre  = "\n--".$boundary;
        $headers   = "From: $from";
        $headers  .= "\nReply-To: $from";
        $headers  .= "\nContent-Type: multipart/mixed; boundary=\"".$boundary."\"";
        $message   = $boundWithPre;
        $message  .= "\n Content-Type: text/html; charset=UTF-8\n";
        $message  .= "\n $mailMessage";
        $message .= $boundWithPre;
        $message .= "\nContent-Type: application/octet-stream; name=\"".$attchmentName."\"";
        $message .= "\nContent-Transfer-Encoding: base64\n";
        $message .= "\nContent-Disposition: attachment\n";
        $message .= $attachment;
        $message .= $boundWithPre."--";
        $mailStatus = mail($toAddress, $subject, $message, $headers);

        // echo"<pre>"; print_r($mailStatus);exit;
        if($mailStatus == 1){
            $letter->letter_status = $mailStatus ? 1: 0;
            $letter->save();
            $response[ 'success' ] = 1;
            $response[ 'msg' ] = 'Letter have been sent successfully.';
            $response[ 'mailStatus' ]   = $mailStatus."__".date("d/m/Y H.i.s");
        }else{
            $response[ 'success' ] = 0;
            $response[ 'msg' ] = 'Letter have been not sent successfully';
            $this->humbleFaxError( $letter );
        }
        echo json_encode( $response,JSON_UNESCAPED_SLASHES );
    }

    public function humbleFaxError( $letter ){
        DB::table('failed_letters')->insert(
            ['failed_letter_id' => $letter->id, 'letter_author_id' => $letter->user_id, 'letter_author_email' => $letter->user_email, 'error_mesage' => 'Letter have been not sent successfully']
        );
        $find = array(
            '[user_name]',
        );
        $replace = array(
            'user_name' => $letter->user_name,
        );

        $adminsettings = DB::table('admin_settings')->where('id', '1')->get();
        $admin_email = $adminsettings[0]->admin_email;
        $email_template_id = $adminsettings[0]->email_template_id;

        $emailtemplates = DB::table('emailtemplates')->where('id', $email_template_id)->get();
        $subject = $emailtemplates[0]->subject;
        $format = html_entity_decode($emailtemplates[0]->message, ENT_QUOTES, 'UTF-8');

        $failure_message = str_replace(array("\r\n", "\r", "\n"), '', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '', trim(str_replace($find, $replace, $format))));
        // Mail Send
        $from = $admin_email;
        $to = $letter->user_email;
        $subject = $subject;
        $message = $failure_message;
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        $headers .= "From:" . $from . "\r\n";

        $mail_status = mail($to,$subject,$message, $headers);
    }
}