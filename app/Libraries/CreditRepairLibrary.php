<?php
namespace App\Libraries;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ConnectException;
use Log;
use \App\Libraries\LobLibrary;
use \App\Models\LetterUser;
use \App\Models\LobAddress;
use DB;
class CreditRepairLibrary{
    private $api_key;
    private $api_secret;
    private $base_url;

    public function __construct( $api_object ){
        $this->api_object = $api_object;
        $this->base_url = 'https://app.creditrepaircloud.com/api/';
        $this->client = new Client(array('base_uri' => $this->base_url));
    }

    /**
     * fetchLeadAddress
     *
     * @param
     * @return array
     */
    private function fetchLeadAddress( $letter ){
        // Fetch CRC Detail
        $crc_api_key = trim($this->api_object->crc_key);
        $crc_secret_key = trim($this->api_object->crc_secret);
        $api_url = "{$this->base_url}lead/viewRecord?apiauthkey={$this->api_object->crc_key}&secretkey={$this->api_object->crc_secret}&xmlData=<crcloud><client><id>{$letter->cid}</id></client></crcloud>";
        
        Log::info('Lead api: '.$api_url);
        $response = $this->client->request('POST', $api_url);
        $xml = simplexml_load_string($response->getBody(),'SimpleXMLElement',LIBXML_NOCDATA);        
        
        if($xml->result->errors){
            $result[ 'success' ] = 0;
            $data = (array)$xml->result->errors;
            $result[ 'msg' ] = $data['error_message'];
            DB::table('failed_letters')->insert(
                ['failed_letter_id' => $letter->id, 'letter_author_id' => $letter->user_id, 'letter_author_email' => $letter->user_email, 'error_mesage' => $data['error_message']]
            );  
            $find = array(
                '[user_name]',  
            );
            $replace = array(
                'user_name' => $letter->user_name,
            );
            $adminsettings = DB::table('admin_settings')->where('id', '1')->get();
            $admin_email = $adminsettings[0]->admin_email;
            $email_template_id = $adminsettings[0]->email_template_id;

            $emailtemplates = DB::table('emailtemplates')->where('id', $email_template_id)->get();
            $subject = $emailtemplates[0]->subject;
            $format = html_entity_decode($emailtemplates[0]->message, ENT_QUOTES, 'UTF-8');
            
            $failure_message = str_replace(array("\r\n", "\r", "\n"), '', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '', trim(str_replace($find, $replace, $format))));
            // Mail Send
            $from = $admin_email;
            $to = $letter->user_email;
            $subject = $subject;
            $message = $failure_message;
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= "From:" . $from . "\r\n";

            $mail_status = mail($to,$subject,$message, $headers);
            echo json_encode( $result,JSON_UNESCAPED_SLASHES );
        }else{
            $json = json_encode($xml); // json
            return json_decode($json, true); // array
            
        }
        
    }

    public function saveLeadAddress( $letter, $user_email ){
        Log::info('Lead address: '.$letter->user_id);
        $client_address = $this->fetchLeadAddress( $letter );
        
        Log::info('fetched address: '.print_r( $client_address, true ));
        if( $client_address[ 'success' ] && $client_address[ 'success' ] == 'True' ){
            $client_address[ 'result' ][ 'user_id' ] = $client_address[ 'result' ][ 'id' ];
            unset( $client_address[ 'result' ][ 'id'] );
            $letterUser = new LetterUser;
            foreach( $client_address[ 'result' ] as $field => $vals ){
                $letterUser->{$field} = $vals ? $vals: '';
            }
            $letterUser->save();
            $this->saveAddressToLob( $letterUser, $letter );
        }
    }

    public function saveAddressToLob( $letterUser, $letter){
        $name = trim("{$letterUser->firstname} {$letterUser->middlename} {$letterUser->lastname} {$letterUser->suffix}");
        $address = array(
            'description'       => $letterUser->user_id,
            'name'              => $name,
            'company'           => '',
            'address_line1'     => $letterUser->street_address,
            'address_line2'     => "",
            'address_city'      => $letterUser->city,
            'address_state'     => $letterUser->state,
            'address_country'   => 'US',
            'address_zip'       => $letterUser->zip,
            'email'             => $letterUser->email,
            'phone'             => $letterUser->phone_home
        );

        // require_once base_path().'/vendor/lob/lob-php/vendor/autoload.php';
        // $lob = new LobLibrary( $this->api_object->lob_secret, $this->api_object->lob_push );
        // echo '<pre>';print_r($this->api_object->lob_secret);exit;
        // Log::info('Lead address: '.print_r( $lob, true ));

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://api.lob.com/v1/addresses',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $address,
        CURLOPT_HTTPHEADER => array(
            "Authorization: Basic ".base64_encode($this->api_object->lob_secret.":")
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        
        $addr = (array)json_decode( $response );  
        
        // echo'<pre>';print_r($addr);
        if( isset($addr['error']->status_code ) ){
            $response_array[ 'success' ] = 0;
            $response_array[ 'msg' ] = $addr['error']->message;
            DB::table('failed_letters')->insert(
                ['failed_letter_id' => $letter->id, 'letter_author_id' => $letter->user_id, 'letter_author_email' => $letter->user_email, 'error_mesage' => $addr['error']->message]
            );  
            $find = array(
                '[user_name]',  
            );
            $replace = array(
                'user_name' => $letter->user_name,
            );
            $adminsettings = DB::table('admin_settings')->where('id', '1')->get();
            $admin_email = $adminsettings[0]->admin_email;
            $email_template_id = $adminsettings[0]->email_template_id;

            $emailtemplates = DB::table('emailtemplates')->where('id', $email_template_id)->get();
            $subject = $emailtemplates[0]->subject;
            $format = html_entity_decode($emailtemplates[0]->message, ENT_QUOTES, 'UTF-8');
            
            $failure_message = str_replace(array("\r\n", "\r", "\n"), '', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '', trim(str_replace($find, $replace, $format))));
            // Mail Send
            $from = $admin_email;
            $to = $letter->user_email;
            $subject = $subject;
            $message = $failure_message;
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= "From:" . $from . "\r\n";

            $mail_status = mail($to,$subject,$message, $headers);
            echo json_encode( $response_array,JSON_UNESCAPED_SLASHES ); 
        }else{
            // $addr[ 'lob_id' ] = $addr[ 'id' ];
            // unset( $addr[ 'id'] );
            // echo'<pre>';print_r($addr);

            // Lob Save
            $lobAddr = new LobAddress;

            // $flds = $lobAddr->getAttributes();

            // foreach($flds as $fld => $val){
            //     if( isset( $addr[ $fld ] ) ){
            //         $lobAddr->{$fld} = $addr[ $fld ] ? $addr[ $fld ]: '';
            //     }
            // }
            

            $lobAddr->user_id = $letter->user_id;
            $lobAddr->lob_id = (!empty($addr[ 'id' ]) ? $addr[ 'id' ] : '');
            $lobAddr->description = (!empty($addr[ 'description' ]) ? $addr[ 'description' ] : '');
            $lobAddr->type = 'Client';
            $lobAddr->name = (!empty($addr[ 'name' ]) ? $addr[ 'name' ] : '');
            $lobAddr->company = (!empty($addr[ 'company' ]) ? $addr[ 'company' ] : '');
            $lobAddr->phone = (!empty($addr[ 'phone' ]) ? $addr[ 'phone' ] : '');
            $lobAddr->email = (!empty($addr[ 'email' ]) ? $addr[ 'email' ] : '');
            $lobAddr->address_line1 = (!empty($addr[ 'address_line1' ]) ? $addr[ 'address_line1' ] : '');
            $lobAddr->address_line2 = (!empty($addr[ 'address_line2' ]) ? $addr[ 'address_line2' ] : '');
            $lobAddr->address_city = (!empty($addr[ 'address_city' ]) ? $addr[ 'address_city' ] : '');
            $lobAddr->address_state = (!empty($addr[ 'address_state' ]) ? $addr[ 'address_state' ] : '');
            $lobAddr->address_zip = (!empty($addr[ 'address_zip' ]) ? $addr[ 'address_zip' ] : '');
            $lobAddr->address_country = (!empty($addr[ 'address_country' ]) ? $addr[ 'address_country' ] : '');
            $lobAddr->save();
            
            // Letter Save
            $letterUser->lob_address_ref = $addr[ 'id' ];
            $letterUser->save();
            // echo'<pre>';print_r($lobAddr);exit;
            $response_array[ 'success' ] = 1;
            $response_array[ 'msg' ] = 'Letter have been sent successfully.';
        }
        
    }
}