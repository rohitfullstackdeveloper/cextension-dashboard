<?php
namespace App\Libraries;
use Log;
use Illuminate\Support\Facades\URL;
use \Lob\Lob;
use \App\Models\Letter;
use \App\Models\LetterUser;
use DB;
use \App\Models\LobAddress;
class LobLibrary{
    private $lob,$lob_secret_key;
    public function __construct( $lob_sec_key, $lob_push_key ){
        $this->lob_secret_key  = $lob_sec_key; 
        // $lob_sec_key = env('LOB_SECRET', false);
        // include_once base_path().'/vendor/mpdf/mpdf/vendor/autoload.php';
        $this->lob = new Lob($lob_sec_key, $lob_push_key);
    }

    /**
     * Save dummy data.
     *
     * @param  \Lob\Lob  $lob
     * @return void
     */
    public function storeDummy(){
        $this->lob->letters()->create(array(
            'description'           => 'Demo Letter '.time(),
            'to[name]'              => 'Harry Zhang',
            'to[address_line1]'     => '185 Berry St',
            'to[address_line2]'     => '# 6100',
            'to[address_city]'      => 'San Francisco',
            'to[address_zip]'       => '94107',
            'to[address_state]'     => 'CA',
            'from'                  => 'adr_4f3dce7628497c59',
            'file'                  => 'http://cextension.kvelltechnologies.com/letter_content/1',
            'merge_variables[name]' => 'Harry',
            'color'                 => true
          ));
    }

    /**
     * Save dummy data.
     *
     * @param  \Lob\Lob  $lob
     * @return array| void
     */
    public function getData(){
        return $this->lob->letters()->all(array(
            'limit' => 2
        ));
    }

    /**
     * Get address data.
     *
     * @param  \Lob\Lob  $lob
     * @return array| void
     */
    public function getAddresses(){
        return $this->lob->addresses()->all(array(
            'limit' => 10
          ));
    }

    /**
     * Get address data.
     *
     * @param  \Lob\Lob  $lob
     * @return array| void
     */
    public function createAddresses( $address ){
        Log::info('LOB api: '.print_r( $this->lob, true ));
        return $this->lob->addresses()->create( $address );
    }
    function sendLetter( Letter $letter ){  
        // echo '<pre>';print_r($letter->cid);exit;
        if( $letter->bureauname ){                                                            
            $letterType = $this->getMailType( $letter->letter_format_type );   
            $company = LobAddress::where('name', 'LIKE', "%{$letter->bureauname}%")->where('user_id', '=', $letter->user_id)->first();

            $client = LetterUser::where('user_id', $letter->cid)->first();
            //$lob_address = LobAddress::where('description', $letter->cid)->first();    
            // if( $company && $client && $lob_address && $lob_address->lob_id && $client->lob_address_ref){
            if( $company && $client && $client->lob_address_ref){
                $lead_name = trim("{$client->firstname} {$client->middlename} {$client->lastname}");
                Log::info('Letter PDF name: '.$letter->pdf_file);
                $pdfUrl = URL::to('/'). "/attachment_output/".$letter->pdf_file;

                // Attachment
                $attachment = URL::to('/'). "/attachment_output/".$letter->pdf_file;

                Log::info('Letter PDF URL: '.$pdfUrl);
                // echo '<pre>';print_r($client);
                // echo '<pre>';print_r($client->lob_address_ref);exit;
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://api.lob.com/v1/letters',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => array(
                        'description'           =>  $lead_name,
                        "to[name]"              => $company->name,
                        "to[address_line1]"     => $company->address_line1,
                        "to[address_line2]"     => $company->address_line2,
                        "to[address_city]"      => $company->address_city,
                        "to[address_state]"     => $company->address_state,
                        "to[address_zip]"       => $company->address_zip,
                        "from"                  =>  $client->lob_address_ref,
                        'file'                  =>  $pdfUrl,
                        'merge_variables[name]' =>  $lead_name,
                        // 'color'                 =>  false,
                        'address_placement'     =>  'insert_blank_page',
                        'extra_service'         =>  $letterType,
                        'color'                 =>  'true'
                    ),
                    CURLOPT_HTTPHEADER => array(
                        "Authorization: Basic ".base64_encode($this->lob_secret_key.":")
                    ),
                ));
                
                $curl_response = curl_exec($curl);
                curl_close($curl);                
                $lobLetter = (array)json_decode( $curl_response );  
                // echo '<pre>';print_r($lobLetter);exit;
                if( isset($lobLetter['error']->status_code ) ){
                    $response[ 'success' ] = 0;
                    $response[ 'msg' ] = $lobLetter['error']->message;
                    DB::table('failed_letters')->insert(
                        ['failed_letter_id' => $letter->id, 'letter_author_id' => $letter->user_id, 'letter_author_email' => $letter->user_email, 'error_mesage' => $lobLetter['error']->message]
                    );  
                    $find = array(
                        '[user_name]',  
                    );
                    $replace = array(
                        'user_name' => $letter->user_name,
                    );
                    $adminsettings = DB::table('admin_settings')->where('id', '1')->get();
                    $admin_email = $adminsettings[0]->admin_email;
                    $email_template_id = $adminsettings[0]->email_template_id;

                    $emailtemplates = DB::table('emailtemplates')->where('id', $email_template_id)->get();
                    $subject = $emailtemplates[0]->subject;
                    $format = html_entity_decode($emailtemplates[0]->message, ENT_QUOTES, 'UTF-8');
                    
                    $failure_message = str_replace(array("\r\n", "\r", "\n"), '', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '', trim(str_replace($find, $replace, $format))));
                    // Mail Send
                    $from = $admin_email;
                    $to = $letter->user_email;
                    $subject = $subject;
                    $message = $failure_message;
                    
                    // Always set content-type when sending HTML email
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                    $headers .= "From:" . $from . "\r\n";                    
                    $mail_status = mail($to,$subject,$message, $headers);
                }else{
                    $letter->letter_status = 1;
                    if( $lobLetter ){
                        Log::info('LOB api: '.print_r( $lobLetter, true ));
                        $lobLetterId = '';
                        $lobLetterTrackNum = '';
                        if( isset( $lobLetter->id ) ){
                            Log::info('LOB is object ');
                            $lobLetterId = isset( $lobLetter->id ) ? $lobLetter->id : '' ;
                            $lobLetterTrackNum = isset( $lobLetter->tracking_number ) ? $lobLetter->tracking_number : '' ;
                        } else if ( isset( $lobLetter[ 'id' ] ) ){
                            Log::info('LOB is array ');
                            $lobLetterId = isset( $lobLetter[ 'id' ] ) ? $lobLetter[ 'id' ] : '' ;
                            $lobLetterTrackNum = isset( $lobLetter[ 'tracking_number' ] ) ? $lobLetter[ 'tracking_number' ] : '' ;
                        }
                        $letter->lob_ref = $lobLetterId;
                        $letter->lob_trackingnum = $lobLetterTrackNum;
                    }
                    
                    $letter->save();
                    $response[ 'success' ] = 1;
                    $response[ 'msg' ] = 'Letter have been sent successfully.';
                    // DB::table('failed_letter_id')->where('failed_letter_id', $letter->id)->delete();
                }
                echo json_encode( $response,JSON_UNESCAPED_SLASHES ); 
            }else{
                $response[ 'success' ] = 0;
                $response[ 'msg' ] = 'Letter have not been sent';
                DB::table('failed_letters')->insert(
                    ['failed_letter_id' => $letter->id, 'letter_author_id' => $letter->user_id, 'letter_author_email' => $letter->user_email, 'error_mesage' => 'Letter have not been sent']
                );  
                $find = array(
                    '[user_name]',  
                );
                $replace = array(
                    'user_name' => $letter->user_name,
                );
                $adminsettings = DB::table('admin_settings')->where('id', '1')->get();
                $admin_email = $adminsettings[0]->admin_email;
                $email_template_id = $adminsettings[0]->email_template_id;

                $emailtemplates = DB::table('emailtemplates')->where('id', $email_template_id)->get();
                $subject = $emailtemplates[0]->subject;
                $format = html_entity_decode($emailtemplates[0]->message, ENT_QUOTES, 'UTF-8');
                
                $failure_message = str_replace(array("\r\n", "\r", "\n"), '', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '', trim(str_replace($find, $replace, $format))));
                // Mail Send
                $from = $admin_email;
                $to = $letter->user_email;
                $subject = $subject;
                $message = $failure_message;
                // Always set content-type when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= "From:" . $from . "\r\n";
                $mail_status = mail($to,$subject,$message, $headers);
                echo json_encode( $response,JSON_UNESCAPED_SLASHES ); 
            }
        }
    }

    private function getMailType( $letter_type ){
        $types = [];
        $types[ 'Standard' ] = 'registered';
        $types[ 'Certified' ] = 'certified';
        $types[ 'Overnight' ] = 'certified_return_receipt';
        return isset( $types[ $letter_type ] ) ? $types[ $letter_type ] : '';
    }
}