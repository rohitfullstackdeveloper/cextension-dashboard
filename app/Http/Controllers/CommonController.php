<?php
namespace App\Http\Controllers;
use Auth;
use Route;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Symfony\Component\HttpFoundation\Request as SRequest;
use Illuminate\Support\Facades\Session;

class CommonController extends Controller { 
	protected $data = array();
	public $login_user_id = '';
    public function __construct(){
		//echo '--'.$current_route_name = Route::currentRouteName();
		$this->data['site_title'] = config('constants.SITE_NAME');
		$this->data['site_name'] = config('constants.SITE_NAME');
		$url = URL::to('/');
        $this->data['site_full_path']=$url; 
        $this->site_full_path = $url;
		$this->data['page_condition'] = 'default';
    }
    public function slugify($text){
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);
		// trim
		$text = trim($text, '-');
		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);
		// lowercase
		$text = strtolower($text);
		if (empty($text)) {
			return 'n-a';
		}
		return $text;
	}
}
