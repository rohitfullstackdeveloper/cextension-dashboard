<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

use \App\Libraries\LobLibrary;
use \App\Libraries\CreditRepairLibrary;

use \App\Models\Letter;
use \App\Models\LetterUser;
use \App\Models\LobAddress;


class AdminLoginController extends Controller
{
    public function index(){
        $return_data = array();
        $return_data['site_title'] = trans('common.admin_dashboard_title');
        return view('backend/adminlogin', array_merge( $return_data));
    }
}
