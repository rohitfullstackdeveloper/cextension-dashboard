<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use Auth;

use \App\Models\Setting;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Session;

use \App\Models\Emailtemplates;

class AdminSettingController extends CommonController {

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct() {

        parent::__construct(); 

        //$this->middleware('auth');

    }

    /**

     * Display a listing of the resource.

     *

     * @return Response

     */

    public function index() {
		$return_data = array();
        $settings =  '';
        $settings =  Setting::where('id', '=', '1')->paginate(10);
        $return_data['settings'] = $settings;
        return view('backend/admin_setting/index', array_merge($this->data, $return_data));
   }

   public function edit($id){
        $admin_setting = Setting::find($id);
        $emailtemplates =  Emailtemplates::get();
        $return_data = array();
        $return_data['emailtemplates'] = $emailtemplates;
        $return_data['admin_setting'] = $admin_setting;
        $return_data['page_condition'] = 'admin_user_page';
        $return_data['site_title'] = trans('Admin Setting') . ' | ' . $this->data['site_title'];
        // echo'<pre>';print_r($return_data);exit;
        return view('backend/admin_setting/create', array_merge($this->data, $return_data));
    }

    public function update(Request $request, $id){
        $validated = $request->validate([
            'email_template_id' => 'required',//|unique:posts|max:255
            'admin_email' => 'required|email',
        ]);
        $setting = Setting::find($id);
        $setting->email_template_id = $request->email_template_id;
        $setting->admin_email = $request->admin_email;
        $setting->save();
        if ($request->save) {
            return redirect(route('edit.admin_setting',$setting->id))->with('success_message', trans('Updated Successfully'));
        }else{
            return redirect(route('list.admin_setting'))->with('success_message', trans('Updated Successfully'));
        }
    }
}

