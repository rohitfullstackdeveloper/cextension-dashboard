<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use Auth;

use \App\Models\LobAddress;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Session;

use DB;

class LetterStreamController extends CommonController {

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct() {

        parent::__construct(); 

        //$this->middleware('auth');

    }

    /**

     * Display a listing of the resource.

     *

     * @return Response

     */

    public function index() {

		$return_data = array();

        $letter_name = isset($_REQUEST['letter_name']) ? str_replace(' ', '%', $_REQUEST['letter_name']) : '';

        $get_letter_name = isset($_GET['letter_name']) ? $_GET['letter_name'] : '';

        $letter_email = isset($_REQUEST['letter_email']) ? $_REQUEST['letter_email'] : '';

        $get_letter_email = isset($_GET['letter_email']) ? $_GET['letter_email'] : '';
        
        $lob_address_query =  LobAddress::where('email','!=','');

        if($letter_email != ""){

            $lob_address_query->Where('email', 'LIKE', "%{$letter_email}%");
        }

        if($letter_name != ""){            
            $lob_address_query->where('name', 'LIKE', '%'.$letter_name.'%');
            
        }

        $lob_address = $lob_address_query->orderBy('id', 'DESC')->toSql();
        // dd( $lob_address );

        $lob_address = $lob_address_query->paginate(10);

        $return_data['lob_address'] = $lob_address;

        $return_data['get_letter_name'] = $get_letter_name;
        
        $return_data['get_letter_email'] = $get_letter_email;

        return view('backend/letter_stream/index', array_merge($this->data, $return_data));

   }

   public function create(){

        $return_data = array();

        //$return_data['lob_address'] = array();

        $return_data['page_condition'] = 'lob_address';

        $return_data['site_title'] = trans('Lob Address') . ' | ' . $this->data['site_title'];

        return view('backend/letter_stream/create', array_merge($this->data, $return_data));

    }

    public function store(Request $request){//

         $validated = $request->validate([

            'lob_id' => 'required',//|unique:posts|max:255

            'type' => 'required',

            'description' => 'required',

            'name' => 'required',

            'company' => 'required',

        ]);

        $lob_address = new LobAddress();

        $lob_address->lob_id = isset($request->lob_id) ? $request->lob_id : '';

        $lob_address->type = isset($request->type) ? $request->type : '';

        $lob_address->description = isset($request->description) ? $request->description : '';

        $lob_address->name = isset($request->name) ? $request->name : '';

        $lob_address->company = isset($request->company) ? $request->company : '';

        $lob_address->phone = isset($request->phone) ? $request->phone : '';

        $lob_address->email = isset($request->email) ? $request->email : '';

        $lob_address->address_line1 = isset($request->address_line1) ? $request->address_line1 : '';

        $lob_address->address_line2 = isset($request->address_line2) ? $request->address_line2 : '';

        $lob_address->address_city = isset($request->address_city) ? $request->address_city : '';

        $lob_address->address_state = isset($request->address_state) ? $request->address_state : '';

        $lob_address->address_zip = isset($request->address_zip) ? $request->address_zip : '';

        $lob_address->address_country = isset($request->address_country) ? $request->address_country : '';

        $lob_address->save();

        if ($request->save) {

            return redirect(route('edit.letter_stream',$lob_address->id))->with('success_message', trans('Added Successfully'));

        }else{

            return redirect(route('list.letter_stream'))->with('success_message', trans('Added Successfully'));

        }

    }

    public function edit($id){

        $lob_address = LobAddress::find($id);

        $return_data = array();

        $return_data['lob_address'] = $lob_address;

        $return_data['page_condition'] = 'lob_address';

        $return_data['site_title'] = trans('Lob Address') . ' | ' . $this->data['site_title'];

        return view('backend/letter_stream/create', array_merge($this->data, $return_data));

    }

    public function update(Request $request, $id){

        $validated = $request->validate([

            'lob_id' => 'required',//|unique:posts|max:255

            'type' => 'required',

            'description' => 'required',

            'name' => 'required',

            'company' => 'required',

        ]);

        $lob_address = LobAddress::find($id);

        $lob_address->lob_id = isset($request->lob_id) ? $request->lob_id : '';

        $lob_address->type = isset($request->type) ? $request->type : '';

        $lob_address->description = isset($request->description) ? $request->description : '';

        $lob_address->name = isset($request->name) ? $request->name : '';

        $lob_address->company = isset($request->company) ? $request->company : '';

        $lob_address->phone = isset($request->phone) ? $request->phone : '';

        $lob_address->email = isset($request->email) ? $request->email : '';

        $lob_address->address_line1 = isset($request->address_line1) ? $request->address_line1 : '';

        $lob_address->address_line2 = isset($request->address_line2) ? $request->address_line2 : '';

        $lob_address->address_city = isset($request->address_city) ? $request->address_city : '';

        $lob_address->address_state = isset($request->address_state) ? $request->address_state : '';

        $lob_address->address_zip = isset($request->address_zip) ? $request->address_zip : '';

        $lob_address->address_country = isset($request->address_country) ? $request->address_country : '';

        $lob_address->save();

        if ($request->save) {

            return redirect(route('edit.letter_stream',$lob_address->id))->with('success_message', trans('Updated Successfully'));

        }else{

            return redirect(route('list.letter_stream'))->with('success_message', trans('Updated Successfully'));

        }

    }

}

