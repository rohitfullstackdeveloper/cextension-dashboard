<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use Auth;

use \App\Models\Letter;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Session;



class AdminDashboardController extends CommonController {

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct() {

        parent::__construct(); 

        //$this->middleware('auth');

    }

    /**

     * Display a listing of the resource.

     *

     * @return Response

     */

    public function index() {

		$return_data = array();

        $letter_email = isset($_REQUEST['letter_email']) ? $_REQUEST['letter_email'] : '';

        $get_letter_email = isset($_GET['letter_email']) ? $_GET['letter_email'] : '';

        $letter_type = isset($_REQUEST['letter_type']) ? $_REQUEST['letter_type'] : '';
        
        $get_letter_type = isset($_GET['letter_type']) ? $_GET['letter_type'] : '';
        
        $letter_status = isset($_REQUEST['letter_status']) ? $_REQUEST['letter_status'] : '';

        $get_letter_status = isset($_GET['letter_status']) ? $_GET['letter_status'] : '';

        $letters_query =  Letter::where('user_email','!=','');



        // echo '<pre>';print_r($_GET);

        if($letter_email != ""){

            $letters_query->where('user_email','=',html_entity_decode($letter_email));

        }

        if($letter_type != ""){

            $letters_query->where('letter_type','=',$letter_type);

        }

        if($letter_status != ""){

            $letters_query->where('letter_status','=',$letter_status);

        }

        $letters = $letters_query->orderBy('id', 'DESC')->paginate(10);



        if($get_letter_type ==''){
            $get_letter_type = '';
        }else{
            if($get_letter_type == 0){
                $get_letter_type = -1;
            }elseif($get_letter_type == 1){
                $get_letter_type = 1;
            }else{
                $get_letter_type = 2;
            }
        }

        if($get_letter_status == ''){
            $get_letter_status = '';
        }else{
            if($get_letter_status == 0){
                $get_letter_status = -1;
            }elseif($get_letter_status == 1){
                $get_letter_status = 1;
            }else{
                $get_letter_status = '';
            }        
        }
        
        // echo '<pre>';print_r($get_letter_status);
        $return_data['letters'] = $letters;

        $return_data['get_letter_email'] = $get_letter_email;

        $return_data['get_letter_type'] = $get_letter_type;

        $return_data['get_letter_status'] = $get_letter_status;

        return view('backend/admindashboard', array_merge($this->data, $return_data));

   }

}

