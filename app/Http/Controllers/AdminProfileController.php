<?php
namespace App\Http\Controllers;
use App\Http\Requests;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use View;

class AdminProfileController extends CommonController {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */ 
    
     public function getReset($token = null) {
        $return_data = array();
        $return_data['site_title'] = $this->data['site_title'];
		$return_data['page_condition'] = 'change_password_page';
		$return_data['site_title'] = trans('Change Password') . ' | ' . $this->data['site_title'];
        return view('backend.reset',array_merge($this->data, $return_data));
    }
    public function postReset(Request $request){   
		$this->validate($request, [
				'password' => 'required|confirmed',
		]);
		$credentials = $request->only(
				'password', 'password_confirmation' 
		);
		$user = \Auth::user(); 
		$user->password = bcrypt($credentials['password']);
		$user->save();
		return redirect(route('admin.changepassword'))->with('success_message', trans('Password Change Successfully'));
	}
}
