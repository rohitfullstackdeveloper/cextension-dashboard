<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use Auth;

use \App\Models\Humblefax;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Session;



class HumblexAddressController extends CommonController {

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct() {

        parent::__construct(); 

        //$this->middleware('auth');

    }

    /**

     * Display a listing of the resource.

     *

     * @return Response

     */

    public function index() {

		$return_data = array();

        $letter_name = isset($_REQUEST['letter_name']) ? $_REQUEST['letter_name'] : '';

        $get_letter_name = isset($_GET['letter_name']) ? $_GET['letter_name'] : '';

        $letter_email = isset($_REQUEST['letter_email']) ? $_REQUEST['letter_email'] : '';

        $get_letter_email = isset($_GET['letter_email']) ? $_GET['letter_email'] : '';

        $lettername = str_replace(' ', '%', $letter_name);
        
        $humblex_address_query =  Humblefax::where('name','!=','');

        if($letter_email != ""){

            $humblex_address_query->Where('faxemail', 'LIKE', "%{$letter_email}%");
        }

        if($letter_name != ""){
            
            $humblex_address_query->where('name', 'LIKE', '%'.$lettername.'%');
            
        }

        $humblex_address = $humblex_address_query->orderBy('id', 'DESC');

        $humblex_address = $humblex_address_query->paginate(10);

        // echo '<pre>';print_r($letters);exit;

        $return_data['humblex_address'] = $humblex_address;

        $return_data['get_letter_name'] = $get_letter_name;
        
        $return_data['get_letter_email'] = $get_letter_email;

        return view('backend/humblex_address/index', array_merge($this->data, $return_data));

   }

   public function create(){

        $return_data = array();

        //$return_data['humblex_address'] = array();

        $return_data['page_condition'] = 'humblex_address';

        $return_data['site_title'] = trans('Humblex Address') . ' | ' . $this->data['site_title'];

        return view('backend/humblex_address/create', array_merge($this->data, $return_data));

    }

    public function store(Request $request){//

         $validated = $request->validate([

            'name' => 'required',

        ]);

        $humblex_address = new Humblefax();

        $name = $humblex_address->name = isset($request->name) ? $request->name : '';

        $humblex_address->name = $name;

        $slug = $this->slugify($name);

        $humblex_address->slug = $slug;

        $humblex_address->faxnum = isset($request->faxnum) ? $request->faxnum : '';

        $humblex_address->faxemail = isset($request->faxemail) ? $request->faxemail : '';

        $humblex_address->save();

        if ($request->save) {

            return redirect(route('edit.humblex_address',$humblex_address->id))->with('success_message', trans('Added Successfully'));

        }else{

            return redirect(route('list.humblex_address'))->with('success_message', trans('Added Successfully'));

        }

    }

    public function edit($id){

        $humblex_address = Humblefax::find($id);

        $return_data = array();

        $return_data['humblex_address'] = $humblex_address;

        $return_data['page_condition'] = 'humblex_address';

        $return_data['site_title'] = trans('Humblex Address') . ' | ' . $this->data['site_title'];

        return view('backend/humblex_address/create', array_merge($this->data, $return_data));

    }

    public function update(Request $request, $id){

        $validated = $request->validate([

            'name' => 'required',//|unique:posts|max:255

        ]);

        $humblex_address = Humblefax::find($id);

        $name = $humblex_address->name = isset($request->name) ? $request->name : '';

        $humblex_address->name = $name;

        $slug = $this->slugify($name);

        $humblex_address->slug = $slug;

        $humblex_address->faxnum = isset($request->faxnum) ? $request->faxnum : '';

        $humblex_address->faxemail = isset($request->faxemail) ? $request->faxemail : '';

        $humblex_address->save();

        if ($request->save) {

            return redirect(route('edit.humblex_address',$humblex_address->id))->with('success_message', trans('Updated Successfully'));

        }else{

            return redirect(route('list.humblex_address'))->with('success_message', trans('Updated Successfully'));

        }

    }

}

