<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Log;
use \App\Libraries\LobLibrary;
use \App\Libraries\HumbleFaxLibrary;
use \App\Libraries\CreditRepairLibrary;
use \App\Libraries\LetterStreamLibrary;
use \App\Libraries\PostalCityLibrary;
use \App\Models\Letter;
use \App\Models\LetterUser;
use App\Models\PdfJobs;
use DB;
use GuzzleHttp\Client;
use Cache;
use \App\Models\LobAddress;
class LetterController extends Controller{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    private function doAction(Request $request, $action){
        /*echo'<pre>';print_r($request); print_r($_FILES );exit;*/
        if( $request->has('usession') && $request->has('ext_letter_content') && $request->has('cid') ){
            $cid =  $request->cid;
            $lett_subs = $request->lett_subs;
            $bureauname = $request->bureauname;
            $ext_letter_content = $request->ext_letter_content;
            $letterType = "";

            if( $action == 'lob' ){
                $letterFormatType = $request->letter_type;
                $letter_type =  0;
            }else if( $action == 'leterstream' ){
                $letterFormatType = 'Standard';
                $letter_type =  2;
            }else if( $action == 'postalocity' ){
                $letterFormatType = 'Standard';
                $letter_type =  3;
            }else{
                $letterFormatType = 'Standard';
                $letter_type =  1;
            }

            $attachments =  array();

            if( $cid && $ext_letter_content ){
                /*
                if(is_array($_FILES) && !empty($_FILES)){
                    $index = 1;
                    if( $action == 'fax' ){
                        $attached_documents = $_FILES['hf_attachment']['name'];
                        $attached_tmp = $_FILES['hf_attachment']['tmp_name'];
                    }else{
                        $attached_documents = $_FILES['attachment']['name'];
                        $attached_tmp = $_FILES['attachment']['tmp_name'];
                    }

                    foreach ($attached_documents as $key => $value) {
                        $filename = 'attachment-'.time().'-'.$index.'.pdf';
                        $targetPath = base_path()."/public/mpdf/attachment/".$filename;
                        $sourcePath = $attached_tmp[$key];
                        // echo '<pre>';print_r($app_filename);exit;
                        
                        $app_url = base_path()."/public/mpdf/attachment/".$filename;
                        move_uploaded_file($sourcePath, $targetPath);
                        $attachments[] =  $filename;
                        $index++;
                    }
                    
                    
                    $attachment_string = implode(',',$attachments);
                    // return response($attachment_string)->setStatusCode(201);exit;
                }else{
                    $attachment_string = '';
                }
                */
                
                for( $i = 0; $i < count( $cid ); $i++ ){                    
                    $letterBody = isset( $ext_letter_content[ $i ] ) ? $ext_letter_content[ $i ] : '';
                    $letter = new Letter;
                    $letter->cid = $cid[ $i ];
                    $letter->usession = $request->usession;
                    $letter->subject = isset( $lett_subs[ $i ] ) ? $lett_subs[ $i ] : '';
                    $letter->bureauname = isset( $bureauname[ $i ] ) ? $bureauname[ $i ] : '';
                    $letter->letter_format_type = $letterFormatType;
                    $letter->letter_type = $letter_type;
                    $letter->user_email = $request->user_email;
                    $letter->user_id = $request->user_id;
                    $letter->user_name = $request->user_name;
                    $letter->letter_status = 0;
                    $letter->ext_letter_content = $letterBody;
                    
                    
                    if(is_array($_FILES) && !empty($_FILES)){
                        $attachments = array();
                        $attached_documents = $_FILES['attachment']['name'][$cid[$i]][$bureauname[$i]];
                        $attached_tmp = $_FILES['attachment']['tmp_name'][$cid[$i]][$bureauname[$i]];
                        $index = 1;    
                        if( !empty( $attached_documents )){
                            foreach ($attached_documents as $key => $value) {                                
                                $filename = 'attachment-'.time().'-'.$index.'.pdf';
                                $targetPath = base_path()."/public/mpdf/attachment/".$filename;
                                $sourcePath = $attached_tmp[$key];
                                // echo '<pre>';print_r($app_filename);exit;
    
                                $app_url = base_path()."/public/mpdf/attachment/".$filename;
                                move_uploaded_file($sourcePath, $targetPath);
                                $attachments[] =  $filename;
                                $index++;
                            }
                            $attachment_string = implode(',',$attachments);
                        }else{
                            $attachment_string = '';           
                        }
                    }else{
                        $attachment_string = '';       

                    }
                    $letter->attachments = $attachment_string;
                    $letter->save();
                    Log::info('Letter entered: '.$letter->cid);


                    //$letterUser = LetterUser::where('user_id', $letter->cid)->first();;
                    //Log::info('Letter user: '.print_r( $letterUser, true ));
                    //if( !$letterUser ){
                    //    Log::info('Letter user creation: '.$letter->cid);
                    //    $cletter = new CreditRepairLibrary();
                    //    $cletter->saveLeadAddress( $letter->cid );
                    //    $this->sendCreatePDF( $letter->id, $action);
                    //    continue;
                    //}
                    //$this->sendCreatePDF( $letter->id, $action );
                }

                $response =  array( 'response' => 1, 'message' => "Letter submitted successfully");
                return response(json_encode($response))->setStatusCode(200);
            }
        }

        // return response("Letter submitted successfully")->setStatusCode(201);

        return response("Letter incomplete")->setStatusCode(400);
    }

    public function testletter(Request $request){
        $lob_secret_key = 'test_f58dde8d885031dff503e525bf51aebfcaa';
        $client_lob_address_ref = 'adr_067e194adc809087';
        $pdfUrl = 'Letter_94.pdf';
        $fileName = URL::to('/')."/mpdf/".$pdfUrl;
        $curl = curl_init();
        $firstname = 'Godwin';
        $middlename = '';
        $lastname = 'Nwokocha';
        $lead_name = trim("{$firstname} {$middlename} {$lastname}");
        $letterType = 'registered';
        $company_name  = 'ASHLEY TURNER';
        $address_line1 = '7915 ALTA CT';
        $address_line2 = '';
        $address_city  = 'LITHIA SPRINGS';
        $address_state = 'GA';
        $address_zip = '30122-7817';

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.lob.com/v1/letters',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array(
                'description'           =>  $lead_name,
                "to[name]"              => $company_name,
                "to[address_line1]"     => $address_line1,
                "to[address_line2]"     => $address_line2,
                "to[address_city]"      => $address_city,
                "to[address_state]"     => $address_state,
                "to[address_zip]"       => $address_zip,
                "from"                  =>  $client_lob_address_ref,
                'file'                  =>  $fileName,
                'merge_variables[name]' =>  $lead_name,
                'attachment'            =>  '<html style="padding-top: 3in; margin: .5in;">HTML Letter for {{name}}</html>',
                'address_placement'     =>  'insert_blank_page',
                'extra_service'         =>  $letterType,
                'color'                 =>  'true'
            ),
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic ".base64_encode($lob_secret_key.":")
            ),
        ));

        $curl_response = curl_exec($curl);
        echo $curl_response.'<br/>';
        curl_close($curl);
        $lobLetter = (array)json_decode( $curl_response );
        echo '<pre>'; print_r( $lobLetter ); exit;
    }

    public function testHumbleFaxLetter(Request $request){
        /*
        $letterUser = LetterUser::where( 'user_id', $letter->cid )->first();
        $name = trim("{$letterUser->firstname} {$letterUser->middlename} {$letterUser->lastname} {$letterUser->suffix}");
        $from = $letterUser->email;
        $from = $this->from_humblefax_email;
        $toAddress = $this->destination->faxemail;
        $subject = "Testing";
        $file = URL::to('/'). "/mpdf/".$letter->pdf_file;
        $mailMessage = '<html>
                        <head>
                            <title>'.$name.'</title>
                        </head>
                        <body>
                        <p>Sender Address!</p>
                        <table>
                            <tr> <td>Name</td><td>'.$name.'</td> </tr>
                            <tr> <td>Country</td><td>US</td> </tr>
                            <tr> <td>Address</td><td>'.$letterUser->street_address.'</td> </tr>
                            <tr> <td>City</td><td>'.$letterUser->city.'</td> </tr>
                            <tr> <td>State</td><td>'.$letterUser->state.'</td> </tr>
                            <tr> <td>Zip</td><td>'.$letterUser->zip.'</td> </tr>
                            <tr> <td>Email</td><td>'.$letterUser->email.'</td> </tr>
                            <tr> <td>Phone</td><td>'.$letterUser->phone_home.'</td> </tr>
                        </table>
                        </body>
                    </html>';

        $subject = "$name {$this->destination->name}";
        $fileAttachment = trim($file);
        $pathInfo       = pathinfo($fileAttachment);
        $attchmentName  = "attachment_".date("YmdHms").( (isset($pathInfo['extension']))? ".".$pathInfo['extension'] : ""
        );
        $attachment    = chunk_split(base64_encode(file_get_contents($fileAttachment)));
        $boundary      = "PHP-mixed-".md5(time());
        $boundWithPre  = "\n--".$boundary;
        $headers   = "From: $from";
        $headers  .= "\nReply-To: $from";
        $headers  .= "\nContent-Type: multipart/mixed; boundary=\"".$boundary."\"";
        $message   = $boundWithPre;
        $message  .= "\n Content-Type: text/html; charset=UTF-8\n";
        $message  .= "\n $mailMessage";
        $message .= $boundWithPre;
        $message .= "\nContent-Type: application/octet-stream; name=\"".$attchmentName."\"";
        $message .= "\nContent-Transfer-Encoding: base64\n";
        $message .= "\nContent-Disposition: attachment\n";
        $message .= $attachment;
        $message .= $boundWithPre."--";
        $mailStatus = mail($toAddress, $subject, $message, $headers);
        echo 'Mail Status='.$mailStatus; exit;
        */
    }

    public function sendletter(Request $request){
        // Mail type
        $mail_type = $request['mail_type'] ? $request['mail_type'] : '';
        return $this->doAction( $request, $mail_type );
    }

    public function sendfax(Request $request ){
        return $this->doAction( $request, "fax" );
    }

    private function sendCreatePDF( $letter_id, $action,$user_detail ){
        $letter = Letter::find( $letter_id );

        if( $letter->id && $letter->ext_letter_content ){
            require_once base_path().'/vendor/mpdf/mpdf/vendor/autoload.php';
            Log::info('Letter PDF creation: '.$letter->cid);
            $mpdf = new \Mpdf\Mpdf( ['mode' => 'utf-8', 'format' => [215.89, 279.4] ] );
            $mpdf->WriteHTML( $letter->ext_letter_content );
            $pdfFile = "Letter_{$letter->id}.pdf";
            Log::info('Letter PDF PATH: '."app/public/attachment_output/".$pdfFile);
            $fileName = base_path()."/public/attachment_output/".$pdfFile;
            $fileNumber  = 1;
            $mpdf->Output( $fileName, 'F' );

            if( !empty( $letter->attachments )){
                $merge_pdfs[] = $fileName;

                // Get Attachments
                $attachments = explode(',',$letter->attachments);

                foreach( $attachments as $attachment ){
                    $merge_pdfs[] = base_path()."/public/mpdf/attachment/".$attachment;
                }

                $mpdf = new \Mpdf\Mpdf( ['mode' => 'utf-8', 'format' => [215.89, 279.4] ] );
                $filesTotal = sizeof($merge_pdfs);
                $final_file = "Letter_{$letter->id}_final.pdf";
                $outFile = base_path()."/public/attachment_output/".$final_file;

                foreach ($merge_pdfs as $fileName) {
                    if (file_exists($fileName)) {
                        $pagesInFile = $mpdf->SetSourceFile($fileName);

                        for ($i = 1; $i <= $pagesInFile; $i++) {
                            $tplId = $mpdf->ImportPage($i); // in mPdf v8 should be 'importPage($i)'
                            $mpdf->UseTemplate($tplId);
                            if (($fileNumber < $filesTotal) || ($i != $pagesInFile)) {
                                $mpdf->WriteHTML('<pagebreak />');
                            }
                        }
                    }
                    $fileNumber++;
                }

                $mpdf->Output($outFile,'F');
            }else{
                $final_file = $pdfFile;
            }

            $letter->pdf_file = $final_file;
            $letter->save();
            $response = '';

            if( $action == 'lob' ){
                require_once base_path().'/vendor/lob/lob-php/vendor/autoload.php';
                $wpklllob_secretkey = $user_detail->lob_secret;
                $wpklllob_pushkey = $user_detail->lob_push;
                $lob = new LobLibrary( $wpklllob_secretkey, $wpklllob_pushkey );
                $response  = $lob->sendLetter( $letter );
            }
            if( $action == 'leterstream' ){
                require_once base_path().'/vendor/lob/lob-php/vendor/autoload.php';
                $wpklllob_api_id = $user_detail->letterstream_api_id;
                $wpklllob_api_key = $user_detail->letterstream_api_key;
                $letterstream = new LetterStreamLibrary( $wpklllob_api_id, $wpklllob_api_key );
                $response  =  $letterstream->sendLetterStream( $letter );
            }
            
            if( $action == 'postalocity' ){
                // Get Postaocity details
                $postcity_details = $user_detail->postalocity_details;
                if(empty($postcity_details)){
                    $response[ 'success' ] = 0;
                    $response[ 'msg' ] = 'Letter have not been sent';
                    DB::table('failed_letters')->insert(
                        ['failed_letter_id' => $letter->id, 'letter_author_id' => $letter->user_id, 'letter_author_email' => $letter->user_email, 'error_mesage' => 'Letter have not been sent']
                    );  
                    $find = array(
                        '[user_name]',  
                    );
                    $replace = array(
                        'user_name' => $letter->user_name,
                    );
                    $adminsettings = DB::table('admin_settings')->where('id', '1')->get();
                    $admin_email = $adminsettings[0]->admin_email;
                    $email_template_id = $adminsettings[0]->email_template_id;
    
                    $emailtemplates = DB::table('emailtemplates')->where('id', $email_template_id)->get();
                    $subject = $emailtemplates[0]->subject;
                    $format = html_entity_decode($emailtemplates[0]->message, ENT_QUOTES, 'UTF-8');
                    
                    $failure_message = str_replace(array("\r\n", "\r", "\n"), '', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '', trim(str_replace($find, $replace, $format))));
                    // Mail Send
                    $from = $admin_email;
                    $to = $letter->user_email;
                    $subject = $subject;
                    $message = $failure_message;
                    // Always set content-type when sending HTML email
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                    $headers .= "From:" . $from . "\r\n";
                    $mail_status = mail($to,$subject,$message, $headers);
                }else{
                    $postalcity = new PostalcityLibrary( $postcity_details, $letter );
                    $CreateJob_response  =  $postalcity->CreateJob();
                    if( isset($CreateJob_response['type']) && $CreateJob_response['type'] == 'SUCCESS' ){
                        $UpdateJob_response  =  $postalcity->UpdateJob($CreateJob_response['job']['id']);
                        $AddSource_response  =  $postalcity->AddSource($CreateJob_response['job']['id']);
                        // error
                        $response  =  $postalcity->JobStart($CreateJob_response['job']['id']);
                    }
                }
            }

            if( $action == 'fax' && $letter->bureauname){
                $hfax = new HumbleFaxLibrary( $letter, $user_detail->hfax_email  );
                $response = $hfax->sendFax( $letter,$user_detail->user_email );
            }
            echo $response; exit;
        }
    }

    public function getLobLetters( Request $request ){
        if( isset( $request[ 'email' ] )){
            $letters =  Letter::where('letter_type', '=', '0')->where('usession', '=', $request[ 'email' ])->get();
        }else{
            $letters =  Letter::where('letter_type', '=', '0')->get();
        }

        $response =  array();

        if( !empty( $letters)){
            $response[ 'success' ] = 1;
            $response[ 'data' ] = $letters;
            $response[ 'total' ] = count($letters);
        }else{
            $response[ 'success' ] = 0;
        }

        echo json_encode( $response,JSON_UNESCAPED_SLASHES ); exit;
    }

    public function getHumbleLetters( Request $request ){
        if( isset( $request[ 'email' ] )){
            $letters =  Letter::where('letter_type', '=', '1')->where('usession', '=', $request[ 'email' ])->get();
        }else{
            $letters =  Letter::where('letter_type', '=', '1')->get();
        }

        // $letters =Letter::where('letter_type', '=', '1')->get();
        $response =  array();

        if( !empty( $letters)){
            $response[ 'success' ] = 1;
            $response[ 'total' ] = count($letters);
            $response[ 'data' ] = $letters;
        }else{
            $response[ 'success' ] = 0;
        }

        echo json_encode( $response,JSON_UNESCAPED_SLASHES ); exit;
    }

    public function getpendingLetters( Request $request ){
        //  Get All Pending Letters
        $letters = DB::select("select * from  `letters` where letter_status='0' and id NOT IN(select distinct(failed_letter_id) from  `failed_letters`) order by id Desc limit 10");
        //echo '<pre>'; print_r($letters); exit;
        if(count($letters) > 0){
            foreach($letters as $letter){
                //  Get User Email
                $user_email  =  $letter->user_email;
                $user_id  =  $letter->user_id;

                //  Get User Detail
                $user_detail = DB::table('user_settings')->where('user_email', $user_email)->where('user_id', $user_id)->first();

                $postcity_details = '';

                if($letter->letter_type == 0){
                    $action = 'lob';
                }else if ($letter->letter_type == 2){
                    $action = 'leterstream';
                }else if ($letter->letter_type == 3){
                    $action = 'postalocity';
                    //  Get Postal City Detail
                    $postcity_details = DB::table('postalocity_details')->where('user_email', $user_email)->first();
                    $user_detail->postalocity_details  = (!empty($postcity_details))?$postcity_details:array();
                }else{
                    $action = 'fax';
                }

                $letterUser = LetterUser::where('user_id', $letter->cid)->first();
                Log::info('Letter user: '.$letterUser);

                if( !$letterUser ){
                    Log::info('Letter user creation: '.$letter->cid);
                    $cletter = new CreditRepairLibrary($user_detail);
                    $cletter->saveLeadAddress( $letter,$user_email );
                }

                $this->sendCreatePDF( $letter->id, $action,$user_detail );
            }
        }
    }

    public function getHumblebyLetterStatus( Request $request ){
        $letters =  Letter::where( 'letter_type', '=' ,'1' );

        if( $request[ 'letter_status' ]){
            $letters->where('letter_status', '=', $request[ 'letter_status' ]);
        }

        if( $request[ 'user_email' ]){
            $letters->where('user_email', '=', html_entity_decode($request[ 'user_email' ]));
        }

        //  Get Results
        $results = $letters->get();
        $response =  array();

        if( !empty( $letters)){
            $response[ 'success' ] = 1;
            $response[ 'total' ] = count($results);
            $response[ 'data' ] = $results;
        }else{
            $response[ 'success' ] = 0;
        }

        echo json_encode( $response,JSON_UNESCAPED_SLASHES ); exit;
    }

    public function getLobbyLetterStatus( Request $request ){
        $letters =  Letter::where( 'letter_type', '=' ,'0' );

        if( $request[ 'letter_status' ]){
            $letters->where('letter_status', '=', $request[ 'letter_status' ]);
        }

        if( $request[ 'user_email' ]){
            $letters->where('user_email', '=', html_entity_decode($request[ 'user_email' ]));
        }

        if( $request[ 'letter_format_type' ]){
            $letters->where('letter_format_type', '=', $request[ 'letter_format_type' ]);
        }

        //  Get Results
        $results = $letters->get();
        $response =  array();

        if( !empty( $letters)){
            $response[ 'success' ] = 1;
            $response[ 'total' ] = count($results);
            $response[ 'data' ] = $results;
        }else{
            $response[ 'success' ] = 0;
        }

        echo json_encode( $response,JSON_UNESCAPED_SLASHES ); exit;
    }

    public function tmpQueue(){
        $pdfJobs = new PdfJobs;
        $pdfJobs->user_id = 0;
        $pdfJobs->letter_id = 1;
        $pdfJobs->action = 'letter';
        $pdfJobs->status = '1';
        $pdfJobs->save();

        $pdfJobs = new PdfJobs;
        $pdfJobs->user_id = 0;
        $pdfJobs->letter_id = 2;
        $pdfJobs->action = 'fax';
        $pdfJobs->status = '1';
        $pdfJobs->save();
    }
}