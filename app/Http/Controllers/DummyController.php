<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

use \App\Libraries\LobLibrary;
use \App\Libraries\CreditRepairLibrary;

use \App\Models\Letter;
use \App\Models\LetterUser;
use \App\Models\LobAddress;


class DummyController extends Controller
{
    public function saveLobAddress( LobLibrary $lob ){
        $addresses = [];
        $addresses[] = array(
            'description'       => 'Equifax',
            'name'              => 'Equifax',
            'company'           => 'Equifax',
            'address_line1'     => 'Equifax Information Services LLC',
            'address_line2'     => 'P.O. Box 740256',
            'address_city'      => 'Atlanta',
            'address_state'     => 'GA',
            'address_country'   => 'US',
            'address_zip'       => '30374-0256',
            'email'             => '',
            'phone'             => '(888) 826-0598'
        );

        $addresses[] = array(
            'description'       => 'Experian',
            'name'              => 'Experian',
            'company'           => 'Experian',
            'address_line1'     => 'Experian',
            'address_line2'     => 'P.O. Box 4500',
            'address_city'      => 'Allen',
            'address_state'     => 'TX',
            'address_country'   => 'US',
            'address_zip'       => '75013',
            'email'             => '',
            'phone'             => '(972) 390-4925'
        );

        $addresses[] = array(
            'description'       => 'TransUnion',
            'name'              => 'TransUnion',
            'company'           => 'TransUnion',
            'address_line1'     => 'TransUnion LLC Consumer Dispute Center',
            'address_line2'     => 'P.O. Box 2000',
            'address_city'      => 'Chester',
            'address_state'     => 'PA',
            'address_country'   => 'US',
            'address_zip'       => '19016',
            'email'             => '',
            'phone'             => '(770) 980-9414'
        );

        $lbAddr = [];
        foreach( $addresses as $address ){
            $lbAddr[] = $lob->createAddresses( $address );
        }
        foreach( $lbAddr as $addr ){
            $addr[ 'lob_id' ] = $addr[ 'id' ];
            unset( $addr[ 'id'] );
            $lobAddr = new LobAddress;
            $flds = $lobAddr->getAttributes();
            foreach($flds as $fld => $val){
                if( isset( $addr[ $fld ] ) ){
                    $lobAddr->{$fld} = $addr[ $fld ] ? $addr[ $fld ]: '';
                }
            }
            $lobAddr->save();
        }
    }
}
