<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use \App\Models\Emailtemplates;
use Illuminate\Http\Request;
use App\Http\Requests\EmailRequest;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\RedirectResponse;
use Input;
class EmailtemplateController extends CommonController {
	public function __construct() {
        parent::__construct();
		$this->middleware('auth');
	}
	public function index(){
		$email = Emailtemplates::where('deleted','=','0')->orderBy('id', 'DESC')->paginate(10);
		$return_data = array();
		$return_data['page_condition'] = 'emailtemplate';
        $return_data['site_title'] = trans('Email Template');
		$return_data['emaillist'] = $email;
		return view('backend/emailtemplate/index', array_merge($this->data, $return_data));
	}
	public function create(){
		$return_data = array();
		$return_data['email_templete'] = array();
		$return_data['page_condition'] = 'emailtemplate';
		$return_data['site_title'] = trans('Email Template') . ' | ' . $this->data['site_title'];
		return view('backend/emailtemplate/create', array_merge($this->data, $return_data));
	}
	public function store(Request $request){//
		 $validated = $request->validate([
	        'name' => 'required',//|unique:posts|max:255
	        'subject' => 'required',
	        'message' => 'required',
	    ]);
		$emailtemplate = new Emailtemplates();
		$emailtemplate->name = $request->name;
		$emailtemplate->subject = $request->subject;
		$emailtemplate->message = $request->message;
		$emailtemplate->save();
        if ($request->save) {
            return redirect(route('edit.emailtemplate',$emailtemplate->id))->with('success_message', trans('Added Successfully'));
        }else{
            return redirect(route('list.emailtemplate'))->with('success_message', trans('Added Successfully'));
        }
	}
	public function edit($id){
		$emailtemplate = Emailtemplates::find($id);
		$return_data = array();
		$return_data['emailtemplate'] = $emailtemplate;
		$return_data['page_condition'] = 'emailtemplate';
		$return_data['site_title'] = trans('Email Template') . ' | ' . $this->data['site_title'];
        return view('backend/emailtemplate/create', array_merge($this->data, $return_data));
	}
	public function update(Request $request, $id){//Email
		$validated = $request->validate([
	        'name' => 'required',//|unique:posts|max:255
	        'subject' => 'required',
	        'message' => 'required',
	    ]);
		$emailtemplate = Emailtemplates::find($id);
		$emailtemplate->name = $request->name;
		$emailtemplate->subject = $request->subject;
		$emailtemplate->message = $request->message;
		$emailtemplate->save();
        if ($request->save) {
            return redirect(route('edit.emailtemplate',$emailtemplate->id))->with('success_message', trans('Updated Successfully'));
        }else{
            return redirect(route('list.emailtemplate'))->with('success_message', trans('Updated Successfully'));
        }
	}
	public function destroy(Request $request, $id){//Email
		// echo '<pre>';print_r($request->delete);exit;
		$emailtemplate = Emailtemplates::find($id);
		$emailtemplate->delete();
        return redirect(route('list.emailtemplate'))->with('success_message', trans('Deleted Successfully'));
	}
}
