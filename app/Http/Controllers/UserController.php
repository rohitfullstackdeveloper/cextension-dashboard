<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Log;
use \App\Libraries\CreditRepairLibrary;
use App\Models\UserSetting;
use \App\Models\LobAddress;
use \App\Models\Humblefax;
use DB;
class UserController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function storeSetting( Request $request ){
        $user_id = $request->get( 'user_id' );
        $user_email = $request->get( 'user_email' );
        $user_detail = UserSetting::where('user_id', $user_id)->where('user_email', $user_email)->first();

        if($user_detail){
            $user_setting_id = $user_detail->id;
            $usersetting = UserSetting::find($user_setting_id);
            $usersetting->user_email = $request->get( 'user_email' );
            $usersetting->crc_key = $request->get( 'crc_key' );
            $usersetting->lob_push = $request->get( 'lob_push' );
            $usersetting->lob_secret = $request->get( 'lob_secret' );
            $usersetting->crc_secret = $request->get( 'crc_secret' );
            $usersetting->hfax_email = $request->get( 'hfax_email' );            
            $usersetting->letterstream_api_id = $request->get( 'letterstream_api_id' );            
            $usersetting->letterstream_api_key = $request->get( 'letterstream_api_key' );            
            $message = 'API settings have been updated successfully';
            DB::table('failed_letters')->where('letter_author_id', $user_id)->delete();
        }else{
            $usersetting = new UserSetting;
            $usersetting->user_email = $request->get( 'user_email' );
            $usersetting->crc_key = $request->get( 'crc_key' );
            $usersetting->user_id = $request->get( 'user_id' );
            $usersetting->lob_push = $request->get( 'lob_push' );
            $usersetting->lob_secret = $request->get( 'lob_secret' );
            $usersetting->crc_secret = $request->get( 'crc_secret' );
            $usersetting->hfax_email = $request->get( 'hfax_email' );
            $usersetting->letterstream_api_id = $request->get( 'letterstream_api_id' );
            $usersetting->letterstream_api_key = $request->get( 'letterstream_api_key' );
            $message = 'API settings have been saved successfully';
            DB::table('failed_letters')->where('letter_author_id', $user_id)->delete();
        }

        if( $usersetting->save() ){
            return response()->json([
                'success' => 1,
                'msg'     => $message
            ]);
        }else{
            return response()->json([
                'success' => 0,
                'msg' => 'API settings have not been saved. Please try again.'
            ]);
        }        
    }

    public function getSetting( Request $request ){
        return UserSetting::where('user_email', $request->get('user_email'))->get();
    }

    public function getSettingStatus( Request $request ){
        return UserSetting::where('user_id', $request->get('user_id'))->get();
    }

    public function storeLobAddress( Request $request ){
        $user_id = $request->get( 'user_id' );
        $email = $request->get( 'email' );
        $lob_detail = LobAddress::where('user_id', $user_id)->where('email', $email)->first();

        $lobAddress = new LobAddress;
        $lobAddress->user_id = $request->get('user_id');
        $lobAddress->type = 'Customer';
        $lobAddress->description = $request->get('name');
        $lobAddress->name = $request->get('name');
        $lobAddress->company = $request->get('name');
        $lobAddress->phone = $request->get('phone');
        $lobAddress->email = $request->get('email');
        $lobAddress->address_line1 = $request->get('address_line1');
        $lobAddress->address_line2 = $request->get('address_line2');
        $lobAddress->address_city = $request->get('address_city');
        $lobAddress->address_state = $request->get('address_state');
        $lobAddress->address_zip = $request->get('address_zip');
        $lobAddress->address_country = $request->get('address_country');
        $lobAddress->save();
        DB::table('failed_letters')->where('letter_author_id', $user_id)->delete();
        return response()->json([
            'success' => true,
            'msg' => 'Address Saved'
        ]);
    }
    
    public function storeStreamAddress( Request $request ){
        $user_id = $request->get( 'user_id' );
        $email = $request->get( 'email' );
        $lob_detail = LobAddress::where('user_id', $user_id)->where('email', $email)->first();

        $lobAddress = new LobAddress;
        $lobAddress->user_id = $request->get('user_id');
        $lobAddress->type = 'Customer';
        $lobAddress->description = $request->get('name');
        $lobAddress->name = $request->get('name');
        $lobAddress->company = $request->get('name');
        $lobAddress->phone = $request->get('phone');
        $lobAddress->email = $request->get('email');
        $lobAddress->address_line1 = $request->get('address_line1');
        $lobAddress->address_line2 = $request->get('address_line2');
        $lobAddress->address_city = $request->get('address_city');
        $lobAddress->address_state = $request->get('address_state');
        $lobAddress->address_zip = $request->get('address_zip');
        $lobAddress->address_country = $request->get('address_country');
        $lobAddress->save();
        DB::table('failed_letters')->where('letter_author_id', $user_id)->delete();
        return response()->json([
            'success' => true,
            'msg' => 'Address Saved',
            'response' => $response
        ]);
    }

    public function getLobByID(Request $request){
        $data_id = $request->get('id');
        $lob_content = LobAddress::where('id', $data_id)->first();
        //echo"<pre>";print_r($lob_content);exit;
        return response()->json([
            'success' => true,
            'msg' => $lob_content
        ]);

    }

    public function getHumbleByID(Request $request){
        $data_id = $request->get('id');
        $lob_content = Humblefax::where('id', $data_id)->first();
        //echo"<pre>";print_r($lob_content);exit;
        return response()->json([
            'success' => true,
            'msg' => $lob_content
        ]);

    }
    public function updateLobAddress( Request $request ){
        //echo "<pre>";print_r($request);exit;
        $data_id = $request->get('id'); 
        $user_id = $request->get('id'); 

        $name  =  $request->get('name'); 
        $phone = $request->get('phone');
        $email = $request->get('email');
        $address_line1 = $request->get('address_line1');
        $address_line2 = $request->get('address_line2');
        $address_city = $request->get('address_city');
        $address_state = $request->get('address_state');
        $address_zip = $request->get('address_zip');
        $address_country = $request->get('address_country');
        
        $values=array('name'=>$name,'phone'=>$phone,'email'=>$email,'address_line1'=>$address_line1,'address_line2'=>$address_line2,'address_city'=>$address_city,'address_state'=>$address_state,'address_zip'=>$address_zip,'address_country'=>$address_country);
        DB::table('lob_addresses')->where('id', $data_id)->update($values);
        DB::table('failed_letters')->where('letter_author_id', $user_id)->delete();
        // $lobAddress = new LobAddress;
        // $lobAddress->$data_id = $request->get('id');
        // $lobAddress->type = 'Customer';
        // $lobAddress->description = $request->get('name');
        // $lobAddress->name = $request->get('name');
        // $lobAddress->company = $request->get('name');
        // $lobAddress->phone = $request->get('phone');
        // $lobAddress->email = $request->get('email');
        // $lobAddress->address_line1 = $request->get('address_line1');
        // $lobAddress->address_line2 = $request->get('address_line2');
        // $lobAddress->address_city = $request->get('address_city');
        // $lobAddress->address_state = $request->get('address_state');
        // $lobAddress->address_zip = $request->get('address_zip');
        // $lobAddress->address_country = $request->get('address_country');      
        // $lobAddress->update();

        return response()->json([
            'success' => true,
            'msg' => 'Address Updated'
        ]);
    }
    public function updateHumbleAddress( Request $request ){
        $data_id = $request->get('id'); 
        $user_id = $request->get('id'); 

        $name  =  $request->get('name'); 
        $faxnum = $request->get('faxnum');
        $email = $request->get('email');
        
        $values=array('name'=>$name,'faxnum'=>$faxnum);
        DB::table('humblefaxes')->where('id', $data_id)->update($values);
        DB::table('failed_letters')->where('letter_author_id', $user_id)->delete();
        
        return response()->json([
            'success' => true,
            'msg' => 'Address Updated'
        ]);
    }

    public function storeFaxAddress( Request $request ){
        $user_id = $request->get( 'user_id' );
        $fax_detail = Humblefax::where('user_id', $user_id)->first();
        $humbleFax = new Humblefax;
        $humbleFax->user_id = $request->get('user_id');
        $humbleFax->name = $request->get('name');
        $humbleFax->slug = str_replace(" ","_", strtolower( trim($request->get('name')) ) );
        $humbleFax->faxnum = trim($request->get('faxnum'));
        $humbleFax->faxemail = trim($request->get('faxnum'))."@humblefax.com";
        $humbleFax->save();
        DB::table('failed_letters')->where('letter_author_id', $user_id)->delete();
        return response()->json([
            'success' => true,
            'msg' => 'Fax Saved'
        ]);
    }

    public  function getFaxAddress( Request $request ){
        $userId = $request->get('user_id');
        return Humblefax::where('user_id', $userId)->paginate(10);
    }

    public  function getFaxAddressLatest( Request $request ){
        $userId = $request->get('user_id');
        return Humblefax::where('user_id', $userId)->latest()->first();
    }

    public function getLobAddress( Request $request ){
        $userId = $request->get('user_id');
        return LobAddress::where('user_id', $userId)->paginate(10);
    }
    
    public function getLobAddressLatest( Request $request ){
        $userId = $request->get('user_id');
        return LobAddress::where('user_id', $userId)->latest()->first();
    }

    public  function deleteFaxAddress( Request $request ){
        $humble_id = $request->get( 'del' );
        $humbleFax = Humblefax::where('id',$humble_id);
        $humbleFax->delete();
        return response()->json([
            'success' => true,
            'msg' => 'Fax deleted'
        ]);
    }

    public function deleteLobAddress( Request $request ){
        $lobAddress_id = $request->get( 'del' );
        $lobAddress = LobAddress::where('id',$lobAddress_id);
        $lobAddress->delete();

        return response()->json([
            'success' => true,
            'msg' => 'Lob Address deleted'
        ]);
    }
}