<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\URL;

use Log;



use \App\Libraries\CreditRepairLibrary;



use \App\Models\Letter;

use \App\Models\LetterUser;

use App\Models\PDFJobs;

use Illuminate\Support\Facades\DB;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;





class UserLetterController extends Controller

{

	function getCount(  Request $request  ){

		$cnt = 0;

		$uId = 0;

		$admin = false;

		if( $request->has('user_id') ){

			$uId = $request->get('user_id');

			$cnt = Letter::where('user_id', $uId)->count();

		}

		if( $request->has('is_admin') ){

			$admin = true;

			$cnt = Letter::count();

		}

		return response()->json([

			'admin' => $admin,

			'count' => $cnt,

			'userId' => $uId

		]);

	}



	function getLetter(Request $request  ){

		// $uId = $request->get('user_id');

		// $letters = [];

		// if( $uId ){

		// 	$letters = Letter::where('user_id', $uId)->paginate(10);

        //     // dd($letters);

        //     // $data = [];

        //     // foreach( $letters->items as $letter ){

        //     //     $tmp = [];

        //     //     $tmp[ 'id' ] = $letter->id;

        //     //     $tmp[ 'user_id' ] = $letter->user_id;

        //     //     $tmp[ 'letter_type' ] = $letter->letter_type;

        //     //     $tmp[ 'pdf_file' ] = $letter->pdf_file;

        //     //     $tmp[ 'pdf_url' ] = $letter->pdf_url;

        //     //     $data[] = $tmp;

        //     // }

        //     // $letters->items = $data;

		// }

		// // return $letters;

		// return response()->json($letters);



        $pdf_url = URL::to("storage/mpdf/");

		$total = 0;

		$per_page = 10;

		$current_page = $request->input("page") ?? 1;

		$starting_point = ($current_page * $per_page) - $per_page;

		$letters = DB::table('letters')

		->join('pdf_jobs', 'pdf_jobs.letter_id', '=', 'letters.id')

		->select('letters.id', 'letters.user_id', 'letters.letter_type', 'letters.pdf_file',  'action');

		if( $request->has('user_id') ){

			$uId = $request->get('user_id');

			$letters = $letters->where('letters.user_id', $uId);

			$total = Letter::where('user_id', $uId)->count();

		}

		if( $request->has('is_admin') ){

			$total = Letter::count();

		}

		$letters = $letters->offset($starting_point)->limit( $per_page )->get();

        $letters = $letters->toArray();

        foreach( $letters as &$letter ){

            $letter->pdf_file = URL::to("storage/mpdf/{$letter->pdf_file}");

        }



        $result = new Paginator($letters, $total, $per_page, $current_page, [

            'path' => $request->url(),

            'query' => $request->query(),

        ]);



        dd($result);

	}
	function myLetters(  Request $request  ){
		$user_email = $request->get('user_email');
        return Letter::Select('user_email','usession','letter_format_type','subject','bureauname','letter_type','letter_status','pdf_file','updated_at')->where('user_email', $user_email)->get();
	}

}

 