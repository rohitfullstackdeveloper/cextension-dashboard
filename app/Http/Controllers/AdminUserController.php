<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use Auth;

use \App\Models\User;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Session;



class AdminUserController extends CommonController {

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct() {

        parent::__construct(); 

    }

    /**

     * Display a listing of the resource.

     *

     * @return Response

     */

    public function index() {

        $return_data = array();

        $letters =  User::select('email')->where('email','!=','')->groupBy('email')->paginate(10);

        $return_data['letters'] = $letters;

        return view('backend/adminuser', array_merge($this->data, $return_data));

   }

   public function edit($id){

        $admin_user = User::find($id);

        $return_data = array();

        $return_data['admin_user'] = $admin_user;

        $return_data['page_condition'] = 'admin_user_page';

        $return_data['site_title'] = trans('Admin User') . ' | ' . $this->data['site_title'];

        return view('backend/admin_user/create', array_merge($this->data, $return_data));

    }

    public function update(Request $request, $id){

        $validated = $request->validate([

            'name' => 'required',//|unique:posts|max:255

            'email' => 'required|email',

        ]);

        $user = User::find($id);

        $user->name = $request->name;

        $user->email = $request->email;

        $user->save();

        if ($request->save) {

            return redirect(route('edit.admin_user',$user->id))->with('success_message', trans('Updated Successfully'));

        }else{

            return redirect(route('list.admin_user'))->with('success_message', trans('Updated Successfully'));

        }

    }

}

