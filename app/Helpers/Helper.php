<?php 
namespace App\Helpers;

class Helper {
    public static function GetmailWiseTotalLetetrs($email){
        $email_letter_count = \App\Models\Letter::where('user_email',$email)->count();
        return $email_letter_count;
    }
    public static function GetmailWiseTotalLobAddresses($email){
        $email_letter_count = \App\Models\Letter::where('user_email',$email)->where('letter_type',0)->count();
        return $email_letter_count;
    }
    public static function GetmailWiseTotalHumbleFaxes($email){
        $email_letter_count = \App\Models\Letter::where('user_email',$email)->where('letter_type',1)->count();
        return $email_letter_count;
    } 
}
