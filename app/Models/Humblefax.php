<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Humblefax extends Model
{

    protected $table = 'humblefaxes';
	public $timestamps = true;
    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'name' => '',
        'slug' => '',
        'faxnum' => '',
        'faxemail' => ''
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'faxnum',
        'faxemail'
    ];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
