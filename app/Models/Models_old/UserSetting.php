<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model
{

    protected $table = 'user_settings';
	public $timestamps = true;

     /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'user_email' => '', 
        'user_id' => '', 
        'crc_key' => '',
        'lob_push' => '', 
        'lob_secret' => '', 
        'crc_secret' => '', 
        'hfax_email' => ''
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_email', 
        'user_id', 
        'crc_key',
        'lob_push', 
        'lob_secret', 
        'crc_secret', 
        'hfax_email'
    ];

    
    protected $hidden = [];

    public function  getPdfUrlAttribute()
    {
        return URL::to("storage/mpdf/{$this->pdf_file}");
    }
}
