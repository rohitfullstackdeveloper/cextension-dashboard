<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LobAddress extends Model
{

    protected $table = 'lob_addresses';
	public $timestamps = true;
    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'lob_id' => '',
        'type' => 'Client',
        'description' => '',
        'name' => '',
        'company' => '',
        'phone' => '',
        'email' => '',
        'address_line1' => '',
        'address_line2' => '',
        'address_city' => '',
        'address_state' => '',
        'address_zip' => '',
        'address_country' => ''
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lob_id',
        'type',
        'description',
        'name',
        'company',
        'phone',
        'email',
        'address_line1',
        'address_line2',
        'address_city',
        'address_state',
        'address_zip',
        'address_country'
    ];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
