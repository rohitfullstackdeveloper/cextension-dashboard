<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PdfJobs extends Model
{

    protected $table = 'pdf_jobs';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        // 'letter_type', 'cid', 'bureauname', 'usession', 'ext_letter_content', 'lob_trackingnum', 'lob_ref'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
