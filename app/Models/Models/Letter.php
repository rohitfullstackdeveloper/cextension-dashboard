<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Letter extends Model{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_email','letter_type', 'cid', 'bureauname', 'usession', 'ext_letter_content', 'lob_trackingnum', 'lob_ref'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function  getPdfUrlAttribute()
    {
        return URL::to("storage/mpdf/{$this->pdf_file}");
    }
}
