<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Emailtemplates extends Model{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'letter_type', 'cid', 'bureauname', 'usession', 'ext_letter_content'
    // ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
