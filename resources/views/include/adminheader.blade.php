<style>
.required{
	color:#FF0000;
}
.right_box {
  margin-top: 15px;
}
.errors_msg{
	color:#FF0000;
}
input[type=file] {
	height: auto;
}
</style>
<div class="top_nav">
	<div class="nav_menu">
		<nav>
			<div class="nav toggle">
				<a id="menu_toggle"><i class="fa fa-bars"></i></a>				
			</div>
			<!-- Date : {{date('Y-m-d h:i:s')}} ({{date_default_timezone_get()}}) -->
			<ul class="nav navbar-nav navbar-right">
			
				<li class="">
					<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<img src="{{url('/') .'/' }}images/user_thunb.png" alt="">
						Admin	<span class=" fa fa-angle-down"></span>
					</a>
					<ul class="dropdown-menu dropdown-usermenu pull-right">
						<li><a href="{{ url('/logout') }}" ><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
					</ul>
				</li>
			</ul>
		</nav>
	</div>
</div>