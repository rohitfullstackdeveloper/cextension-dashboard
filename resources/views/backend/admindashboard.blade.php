@extends('layouts.adminapp')
@section('content')
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Letters</h3>
			</div>
		</div>
		<form action="" method="GET" role="search">

			<div class="input-group">
				<div class="col-sm-6">
					<input type="text" class="form-control mr-2" name="letter_email" placeholder="Search Email" value="<?php echo $get_letter_email; ?>" id="letter_email">
				</div>
				<div class="col-sm-6">
					<select name="letter_type" id="letter_type" class="form-control mr-2">
						<option value="">-- Select Letter Type --</option>
						<option value="0" <?php if(isset($get_letter_type) && $get_letter_type == 0){?>selected="selected"<?php } ?>>Lob</option>
						<option value="1" <?php if(isset($get_letter_type) && $get_letter_type == 1){?>selected="selected"<?php } ?>>HumbleFax</option>
					  </select>
					<!--<input type="text" class="form-control mr-2" name="letter_type" placeholder="Search Letter Type" value="<?php echo $get_letter_type; ?>" id="letter_type">-->
				</div>
				<span class="input-group-btn mr-5 mt-1">
					<button class="btn btn-info" type="submit" title="Search projects">
						<span class="fa fa-search"></span>
					</button>
				</span>
				<span class="input-group-btn">
					<button class="btn btn-danger" type="button" id="reset_dashboard_filters" title="Refresh page">
						<span class="fa fa-refresh	"></span>
					</button>
				</span>
			</div>
		</form>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
                  	<div class="x_content">
						<table id="custom_table" class="table table-bordered" cellspacing="0">
	                      <thead>
	                        <tr>
								<th>Email</th>
								<th>usession</th>
								<th>Letter Format Type</th>
								<th>Subject</th>
								<th>Burea Uname</th>
								<th>Letter Type</th>
	                        </tr>
	                      </thead>
	                      <tbody>
						  		@foreach($letters as $item)
								<tr>
									<td>{{$item->user_email}}</td>
									<td>{{$item->usession}}</td>
									<td>{{$item->letter_format_type}}</td>
									<td>{{$item->subject}}</td>
									<td>{{$item->bureauname}}</td>
									<td>
										@php
										if($item->letter_type == 0){
						                    $action = 'Lob';
						                }else{
						                    $action = 'Humblefax';
						                }
						                @endphp
						                {{$action}}
									</td>
								</tr>
							  @endforeach
						  </tbody>
                    </table>
					</div>
				</div>				
			</div>
		</div>			
		<!-- code--> 
	</div>
</div>

@endsection
