@extends('layouts.adminapp')
@section('content')
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Change Password</h3>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
						<br>
						<form class="form-horizontal" role="form" method="POST" action="{{ route('admin.changepassword') }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="">&nbsp;</label>
								<div class="col-md-8 col-sm-8 col-xs-12">
									@if (count($errors) > 0)
									<div class="alert alert-danger">
										<strong>Whoops!</strong> There were some problems with your input.<br><br>
										<ul>
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="password">Password<span class="required">*</span></label>
								<div class="col-md-8 col-sm-8 col-xs-12">
									<input type="password" class="span6 form-control" name="password" placeholder="" id="password">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="password_confirmation">Confirm Password <span class="required">*</span></label>
								<div class="col-md-8 col-sm-8 col-xs-12">
									<input type="password" class="span6 form-control" name="password_confirmation" id="password_confirmation" placeholder="">
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-3">
									<button type="submit" class="btn btn-primary">Update</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
