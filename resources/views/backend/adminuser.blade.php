@extends('layouts.adminapp')
@section('content')
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>User</h3>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
                  	<div class="x_content">
						<table id="custom_table" class="table table-bordered" cellspacing="0">
	                      <thead>
	                        <tr>
								<th>Email</th>
								<th>Total Letetrs</th>
								<th>Total Lob Addresses</th>
								<th>Total Humble Faxes</th>
	                        </tr>
	                      </thead>
	                      <tbody>
						  		@foreach($letters as $item)
								<tr>
									<td>{{$item->user_email}}</td>
									<td>
										<?php 
										$letter_count = (new \App\Helpers\Helper)->GetmailWiseTotalLetetrs($item->user_email);
										?>
										<a href="{{ url('/backend') }}?search_email={{$item->user_email}}">{{$letter_count}}</a>
									</td>
									<td>
										<?php 
										$Lob_Addresses_count = (new \App\Helpers\Helper)->GetmailWiseTotalLobAddresses($item->user_email);
										?>
										<a href="{{ url('/backend') }}?search_email={{$item->user_email}}&letter_type=0">{{$Lob_Addresses_count}}</a>
									</td>
									<td>
										<?php 
										$Humble_Faxes_count = (new \App\Helpers\Helper)->GetmailWiseTotalHumbleFaxes($item->user_email);
										?>
										<a href="{{ url('/backend') }}?search_email={{$item->user_email}}&letter_type=1">{{$Humble_Faxes_count}}</a>
									</td>
								</tr>
							  @endforeach
						  </tbody>
                    </table>
					</div>
				</div>				
			</div>
		</div>			
		<!-- code--> 
	</div>
</div>
@endsection
