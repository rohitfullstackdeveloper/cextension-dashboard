@extends('layouts.adminapp')
@section('content')
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>@if(isset($emailtemplate))
						Edit Email Template
					@else
						Add Email Template
					@endif
				</h3>
			</div>
			<div class="right_box pull-right">
				<a href="{{route('list.emailtemplate')}}" title="{{trans('common.back')}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i></a>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_content">
						<br>
						{{ csrf_field() }}
						<form class="form-horizontal" method="POST" action="@if(isset($emailtemplate)){{ route('edit.emailtemplate',array('id'=>$emailtemplate->id)) }} @else{{ route('add.emailtemplate') }}@endif" id="emailtemplate-form">
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Name<span class="required">*</span></label>
								<div class="col-md-8 col-sm-8 col-xs-12 ">
									<input type="text" placeholder="" class="form-control" value="{{ old('name', $sms_templete->name) }}" name="name" id="name">
				                    @error('name')<div class="errors_msg">{{ $message }}</div>@enderror
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="subject">Subject<span class="required">*</span></label>
								<div class="col-md-8 col-sm-8 col-xs-12  @if($errors->has('subject')) bad @endif">
									<input type="text" placeholder="" class="form-control" value="{{{ Input::old('subject', isset($emailtemplate) ? $emailtemplate->subject : null) }}}" name="subject" id="subject">
									@if ($errors->has('subject')) <div class="errors_msg">{{ $errors->first('subject') }}</div>@endif
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="message">Message Content<span class="required">*</span></label>
								<div class="col-md-8 col-sm-8 col-xs-12  @if($errors->has('message')) bad @endif">
									<textarea  placeholder="" class="ckeditor form-control" name="message" id="message">{{{ Input::old('message', isset($emailtemplate) ? $emailtemplate->message : null) }}}</textarea>
									@if ($errors->has('message')) <div class="errors_msg">{{ $errors->first('message') }}</div>@endif
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-3">
									<input class="btn btn-sm btn-primary submit" name="save_exit" type="submit" value="{{trans('common.save_exit')}}">
									<input class="btn btn-sm btn-primary submit" name="save" type="submit" value="{{trans('common.save')}}">
									<a href="{{URL::full()}}" class="btn btn-sm btn-warning cancel">{{trans('common.cancel')}}</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
