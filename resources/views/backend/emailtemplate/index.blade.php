@extends('layouts.adminapp')
@section('content')
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Email Template</h3>
			</div>
			<div class="right_box pull-right">
				<a href="{{route('add.emailtemplate')}}" class="btn btn-warning" title="{{trans('common.add_new_btn')}}"><i class="fa fa-plus"></i></a>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">
                    <table id="custom_table" class="table table-bordered" cellspacing="0">
                      <thead>
                        <tr>
                            <th>{{trans('Name')}} </th>
							<th>{{trans('Subject')}} </th>
							<th width="15%">{{trans('common.action')}} </th>
                        </tr>
                      </thead>
                      <tbody>
					  	  @foreach($emaillist as $item)
							<tr>
								<td>{{$item->name}}</td>
								<td>{{$item->subject}}</td>
								<td class="td-actions">
									<a href="{{ route('edit.emailtemplate',array('id'=>$item->id)) }}" rel="tooltip" title="{{trans('common.edit_label')}}" class="btn btn-info btn-xs"><i class="fa fa-pencil"> </i></a>
								</td>
								
							</tr>
						  @endforeach
					  </tbody>
                    </table>
					<div class="dataTables_wrapper no-footer">
                        <div class="dataTables_info" id="example_desc_info" role="status" aria-live="polite">
                            &nbsp;&nbsp;&nbsp;{{trans('common.showing_label')}} {{{ (($emaillist->currentPage() - 1)*$emaillist->perPage()) + 1 }}} to @if(($emaillist->currentPage()*$emaillist->perPage()) >= $emaillist->total()) {{{$emaillist->total() }}} @else  {{{ $emaillist->currentPage()*$emaillist->perPage() }}}  @endif of {{{ $emaillist->total() }}} {{trans('common.items_label')}}
                        </div>
                        <div class="dataTables_paginate paging_simple_numbers" id="example_desc_paginate">
                            {{ $emaillist->appends([])->links() }}
                        </div>
                    </div>
					
                  </div>
                </div>
              </div>
		</div>	
	</div>
</div>
@endsection
