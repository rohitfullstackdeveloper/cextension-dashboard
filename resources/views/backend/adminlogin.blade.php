@extends('layouts.applogin')
@section('content')
<div class="login_wrapper">
	<div class="animate form login_form">
	<section class="login_content">

		<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
		
			<img src="{{url('/') .'/'}}images/logo.png" alt="Admin Panel">
			<p><br /></p>
			<div>
			<input type="text" class="form-control" placeholder="" name="email" value=""/>
			</div>
			<div>
			<input id="password" type="password" class="form-control" placeholder="" name="password">
			</div>
			<div>
			<button type="submit" class="btn btn-default submit">{{ trans('Log in') }}</button>	
			</div>
			
			<div class="clearfix"></div>
		</form>
	</section>
	</div>
</div>
@endsection
