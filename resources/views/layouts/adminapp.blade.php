<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Admin Panel</title>
	<!-- Bootstrap -->
	<link href="{{url('/') .'/admin/css/bootstrap.min.css' }}" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="{{ url('/') .'/admin/css/font-awesome.min.css' }}" rel="stylesheet">
	<!-- NProgress -->
	<link href="{{ url('/') .'/admin/css/nprogress.css' }}" rel="stylesheet">
	<!-- bootstrap-wysiwyg -->
	<link href="{{ url('/') .'/admin/css/prettify.min.css' }}" rel="stylesheet">
	<!-- Custom styling plus plugins -->
	<link href="{{ url('/') .'/admin/css/custom.min.css' }}" rel="stylesheet">
	 <!-- Datatables -->
    <link href="{{ url('/') .'/admin/css/dataTables.bootstrap.min.css' }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ url('/') .'/admin/css/toaster.css' }}" type="text/css" />
	<script type="text/javascript">
    	var base_url = "{{URL::to('/').'/'}}";
    </script> 
	<style>
	.error_msg{
		color:red;
	}
	</style>
</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('include.adminsidebar')
			@include('include.adminheader')
			@yield('content')
			@include('include.adminfooter')
		
	</div>
	@include('include.adminfooterscripts')
	<!-- jQuery -->
	<script type="text/javascript" src="{{ url('/') .'/admin/js/jquery.min.js' }}"></script>
	
	<link rel="stylesheet" href="{{ url('/') .'/admin/css/jquery-ui.css'}}">
	<script src="{{ url('/') .'/admin/js/jquery-ui.js' }}"></script>
	<!--Common -->	
	<script src="{{ url('/') .'/admin/js/toaster.js' }}"></script>
	<!-- Bootstrap -->
	<script src="{{ url('/') .'/admin/js/bootstrap.min.js' }}"></script>
	<!-- FastClick -->
	<script src="{{ url('/') .'/admin/js/fastclick.js' }}"></script>
	<!-- NProgress -->
	<script src="{{ url('/') .'/admin/js/nprogress.js' }}"></script>
	<!-- Custom Theme Scripts -->
	<script src="{{ url('/') .'/admin/js/custom.js' }}"></script>

	 <!-- Datatables -->
	<script src="{{ url('/') .'/admin/js/jquery.dataTables.min.js' }}"></script>
    <script src="{{ url('/') .'/admin/js/custom_load.js' }}"></script>

	 <!-- Dashboard Clear Letters -->
	<script>
		jQuery( "#reset_dashboard_filters" ).click(function() {
		  var url = '/backend';
		  window.location = url;
		});
	</script>
</body>
</html>
