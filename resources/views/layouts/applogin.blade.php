<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Admin Login </title>

    <link href="{{url('/') .'/admin/css/bootstrap.min.css' }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ url('/') .'/admin/css/font-awesome.min.css' }}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ url('/') .'/admin/css/nprogress.css' }}" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="{{ url('/') .'/admin/css/prettify.min.css' }}" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="{{ url('/') .'/admin/css/custom.min.css' }}" rel="stylesheet">
  </head>

  <body class="login">
    <div>
	<a class="hiddenanchor" id="signup"></a>
	<a class="hiddenanchor" id="signin"></a>
	@yield('content')
 </div>
  </body>
</html>